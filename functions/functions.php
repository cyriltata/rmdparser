<?php

/**
 * Gobal Utility functions for RmdParser application
 *
 */
function rmdparser_check_requirements($config) {
	$errors = array();
	if (!file_exists(RMDP_CONFIG_DIR . '/config.php')) {
		$errors[] = sprintf("- Set up config file at %s by copying %s and setting right parameters", RMDP_CONFIG_DIR . '/config.php', RMDP_CONFIG_DIR . '/config.dist.php');
	}

	if (!class_exists('PDO', false)) {
		$errors[] = "- PHP PDO extension is required.";
	}

	if (!function_exists('ldap_connect')) {
		$errors[] = "- PHP openLDAP module is required.";
	}

	if (!function_exists('curl_init')) {
		$errors[] = "- PHP curl extension required.";
	}

	if (!$errors) {
		return true;
	}

	return implode($errors, "\n");
}

function log_message($type, $message) {
	if ($type === 'debug' && RmdParser_Config::get('environment') !== 'dev') {
		return;
	}
	$message = print_r($message, 1);
	error_log("[$type] $message");
}

function log_exception(Exception $e) {
	error_log('RmdP: ' . $e->getMessage());
	error_log('RmdP: ' . $e->getTraceAsString());
}

function show_error($message, $header = null, $heading = '') {
	if ($header && is_numeric($header)) {
		$func = 'header_' . $header;
		call_user_func($func);
	}

	if (!$heading) {
		$heading = "Application Error";
	}
	$message = nl2br(print_r($message, 1));
	include RMDP_APP_DIR . '/views/error.php';
	exit;
}

function header_403() {
	header('HTTP/1.0 403 Forbidden');
}

function header_404() {
	header('HTTP/1.0 404 Not Found');
}

function header_400() {
	header('HTTP/1.0 400 Bad Request');
}

function strip_slashes($str) {
	if (is_array($str)) {
		foreach ($str as $key => $val) {
			$str[$key] = strip_slashes($val);
		}
	} else {
		$str = stripslashes($str);
	}

	return $str;
}

function create_file($filename) {
	$dir = dirname($filename);
	if (!is_dir($dir) && !mkdir($dir)) {
		return false;
	}
	return file_put_contents($filename, "\n");
}

function site_url($uri = '', $echo = true, $query_string = false) {
	$base_url = RmdParser_Config::get('protocol') . RmdParser_Config::get('base_url');
	$index_file = trim(RmdParser_Config::get('index_file'));
	$uri = trim($uri, "/");
	$index_file = trim($index_file, "/");
	if ($index_file) {
		if ($uri) {
			$index_file .= "/?";
		}
		if ($query_string) {
			$uri .= '&' . $query_string;
		}
		$parts = array($base_url, $index_file, $uri);
	} else {
		if ($query_string) {
			$uri .= '/?' . $query_string;
		}
		$parts = array($base_url, $uri);
	}
	$u = implode("/", $parts);
	if ($echo) {
		echo $u;
	}
	return $u;
}

function asset_url($url = '', $echo = true) {
	$base_url = RmdParser_Config::get('protocol') . RmdParser_Config::get('base_url');
	$parts = array($base_url, 'assets', $url);
	$u = implode("/", $parts);
	if ($echo) {
		echo $u;
	}
	return $u;
}

function theme_url($uri, $echo = true) {
	return asset_url('theme/' . $uri, $echo);
}

function admin_url($uri, $echo = true) {
	return site_url('admin/' . $uri, $echo);
}

function redirect($uri = '', $method = 'location', $http_response_code = 302) {
	if (!preg_match('#^https?://#i', $uri)) {
		$uri = site_url($uri);
	}

	switch ($method) {
		case 'refresh' : header("Refresh:0;url=" . $uri);
			break;
		default : header("Location: " . $uri, true, $http_response_code);
			break;
	}
	exit;
}

function hescape($string) {
	return htmlspecialchars($string);
}

function generate_code($length = 64) {
	static $ranges;
	$code = '';
	if ($ranges === null) {
		$ranges = array_merge(range(49, 57), range(65, 90), range(97, 122));
	}
	for ($i = 0; $i < $length; $i++) {
		$code .= chr($ranges[array_rand($ranges)]);
	}
	return $code;
}

function medialab_parse_excel_file($file, $postfix_name) {
	$return = array('success' => false, 'message' => null, 'data' => null);
	$valid_columns = array('name', 'type', 'value', 'label', 'choices', 'class');
	$column_indexes = array();

	try {
		// Set up PHPExcel
		$inputFileType = PHPExcel_IOFactory::identify($file);
		$ExcelReader = PHPExcel_IOFactory::createReader($inputFileType);
		$ExcelReader->setReadDataOnly(true);
		$PHPExcel = $ExcelReader->load($file);
		$sheet = $PHPExcel->getSheet(0);

		// Read rows
		$read_headings = true;
		$columns = array();
		$col = array();
		$i = 0;
		foreach ($sheet->getRowIterator() as $row) {
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);
			$row = array();
			foreach ($cellIterator as $cell) {
				$cellValue = $cell->getValue();
				$colName = $cell->getColumn();
				if ($read_headings) {
					if (!in_array($cellValue, $valid_columns)) {
						throw new Exception("An invalid column '$cellValue' found in excel sheet");
					}
					$column_indexes[$colName] = $cell->getValue();
				} elseif (isset($column_indexes[$colName])) {
					if ($column_indexes[$colName] == 'choices') {
						$row[$column_indexes[$colName]] = medialab_parse_item_choices($cell->getValue());
					} else {
						$row[$column_indexes[$colName]] = trim($cell->getValue());
					}
				}
			}

			if ($row) {
				if ($row['type'] == 'column') {
					$columns[] = $col;
					$col = array();
					continue;
				} elseif ($row['type'] == 'note') {
					$row['index'] = null;
				} elseif ($row['type'] == 'multiple' && $row['choices']) {
					$i = $i + count($row['choices']);
					$row['index'] = $i;
				} else {
					$row['index'] = $i++;
				}
				$row['id'] = md5(microtime(true) . $row['name']) . '-' . $row['name'] . '-item-' . $i;
				$row['postfix_name'] = $postfix_name;
				$col[$row['name']] = $row;
			}
			// after the first row which MUST BE headings, set heading reading to false
			$read_headings = false;
		}

		// Just in case user did not end a column line properly
		if ($col) {
			$columns[] = $col;
		}

		$return['success'] = true;
		$return['data'] = $columns;
	} catch (Exception $e) {
		log_exception($e);
		$return['message'] = $e->getMessage();
	}
	return $return;
}

/**
 * Parse a string defining choices in an exceel sheet. Choices are of the form
 * choice1=value1
 * choice2=value2
 * choice14=value4
 *
 * @param string $text
 * #return array
 */
function medialab_parse_item_choices($text) {
	$text = trim($text);
	$choices = array();
	$array = explode("\n", $text);
	if (!$text || !$array) {
		return $choices;
	}

	foreach ($array as $choice) {
		$choice = trim($choice);
		if (!$choice) {
			continue;
		}
		$fields = explode('=', $choice, 2);
		if (count($fields) == 1) {
			$choice_value = $choice_label = trim($fields[0]);
		} elseif (trim($fields[1]) == '-') {
			$choice_value = $choice_label = trim($fields[0]);
		} else {
			$choice_value = trim($fields[0]);
			$choice_label = trim($fields[1]);
		}
		if ($choice_value == 0 || ($choice_value && $choice_label)) {
			$choices[$choice_value] = $choice_label;
		}
	}
	return $choices;
}

function partnershipstudy_parse_excel_file($file) {
	$return = array('success' => false, 'message' => null, 'data' => array());
	$valid_columns = array('Teilnehmernummer', 'Datum', 'Nachname', 'Vorname', 'Email', 'Studie', 'Gender');
	$column_indexes = array();

	try {
		// Set up PHPExcel
		$inputFileType = PHPExcel_IOFactory::identify($file);
		$ExcelReader = PHPExcel_IOFactory::createReader($inputFileType);
		$ExcelReader->setReadDataOnly(true);
		$PHPExcel = $ExcelReader->load($file);
		$sheet = $PHPExcel->getSheet(0);

		// Read rows
		$read_headings = true;
		$columns = array();
		$col = array();
		$i = 0;
		
		foreach ($sheet->getRowIterator() as $row) {
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);
			$row = array();
			foreach ($cellIterator as $cell) {
				$cellValue = $cell->getValue();
				$colName = $cell->getColumn();
				if ($read_headings) {
					if (!in_array($cellValue, $valid_columns)) {
						throw new Exception("An invalid column '$cellValue' found in excel sheet");
					}
					$column_indexes[$colName] = $cell->getValue();
				} elseif (isset($column_indexes[$colName])) {
					$row[$column_indexes[$colName]] = trim($cell->getValue());
				}
			}

			if ($row && !empty($row['Teilnehmernummer'])) {
				$emails = explode(';', $row['Email']);
				array_walk($emails, 'etrim');

				$return['data'][] = array(
					'id' => $row['Teilnehmernummer'],
					'init_date' => $row['Datum'],
					'first_name' => $row['Vorname'],
					'last_name' => $row['Nachname'],
					'email' => implode(';', $emails),
					'study' => $row['Studie'],
					'gender_greet' => $row['Gender'],
				);
			}
			// after the first row which MUST BE headings, set heading reading to false
			$read_headings = false;
		}

		$return['success'] = true;
	} catch (Exception $e) {
		log_exception($e);
		$return['message'] = $e->getMessage();
	}
	return $return;
}

function partnershipstudy_email_template($template, $data) {
	$vars = array_keys($data);
	foreach ($vars as $var) {
		$template = str_replace('%{'.$var.'}', $data[$var], $template);
	}
	return nl2br($template);
}

function partnershipstudy_get_study_label($study) {
	$labels = RmdParser_Config::get('partnership_study_labels', array());
	return ($study && isset($labels[$study])) ? $labels[$study] : null;
}

function pre($message) {
	echo '<pre>' . print_r($message, 1) . '</pre>';
	exit(0);
}

function etrim(&$str, $id = 0) {
	$str = trim($str);
}
