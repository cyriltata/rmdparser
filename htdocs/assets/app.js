(function ($, undefined) {
	/** Utility Functions */
	function stringTemplate(string, params) {
		for (var i in params) {
			var t = "%?\{" + i + "\}";
			string = string.replace((new RegExp(t, 'g')), params[i]);
		}
		return string;
	}

	function getTemplateById(id, params) {
		var $tpl = jQuery('#' + id);
		if (!$tpl.length)
			return;
		return stringTemplate($.trim($tpl.html()), params);
	}

	function message(title, message) {
		alert(title+': '+message);
	}

	function bs_alert(params, appendTo) {
		appendTo = appendTo || '#bootstrap-alert-container';
		var t = getTemplateById('bootstrap-alert-tpl', params);
		$(appendTo).append(t);
	}

	function redirect(url) {
		document.location = url;
	}

	function post(url, data, callback) {
		$.ajax({
			type: 'POST',
			url: url,
			data: data,
			dataType: 'json',
			success: function (response, code, jqxhr) {
				callback(response);
			},
			error: function (jqxhr, errText) {
				message('Ajax Error', 'There was an error in the ajax request ' + errText);
			},
			beforeSend: function (jqxhr) {
				//alert(this.url);
			}
		});
	}

	/** global variables */
	var $fineuploader;
	var config = window.RmdParserVars || {};

	/** Application */
	var app = {
		initialize: function () {
			if (config.uploads) {
				app.upload.parse(config.uploads, config.force_parse);
			}
			app.fineuploader();
			$('.fileditor').css({height: $(window).height()-50});
		},
		fineuploader: function () {
			$fineuploader = $('.fineuploader').fineUploader({
				request: {
					endpoint: config.uploadUrl,
					params: {is_upload: true},
					uuidName: 'RmdParserFineUploader',
					inputName: 'qqfile'
				},
				text: {
					uploadButton: '<i class="fa fa-file"></i> Select File',
					cancelButton: 'Remove',
					dragZone: '<span class="text-center block">Drop Rmd file here</span> <span class="text-center block">OR</span>'
				},
				classes: {
					fail: 'alert alert-danger',
					success: 'alert alert-success',
					list: 'upload-list',
					progressBar: 'progress-bar'
				},
				validation: {
					itemLimit: 5
				},
				dragAndDrop: {
					hideDropzones: false
				},
				allowedExtensions: ['Rmd'],
				template: getTemplateById('uploader-interface'),
				fileTemplate: getTemplateById('uploader-interface-item'),
				autoUpload: false,
				multiple: true
			}).on('complete', function (event, id, name, response) {
				if (!response || !response.success) {
					delete app.upload.selected[name];
					highlightProgressBar(id, 'danger', ' - ' + (response.error || response.message));
				} else {
					highlightProgressBar(id, 'success');
					app.upload.success[name] = response;
				}

				app.upload.uploaded++;
				if (app.upload.uploaded === app.upload.count) {
					app.upload.completed(app.upload.success);
				}
			}).on('error', function (event, id, name, errorReason, xhr) {
				if (xhr.response && typeof xhr.response.message !== 'undefined') {
					errorReason = xhr.response.message;
				}
				delete app.upload.selected[name];
			}).on('submit', function (event, id, name) {
				app.upload.selected[name] = {file: name};
				$fineuploader.find('#qq-doupload').removeClass('qq-hide');
				$fineuploader.find('#qq-doparse').addClass('qq-hide');
				app.upload.count++;
			}).on('submitted', function (event, id, name) {
				// set id to upload item in list
				$fineuploader.find('.qq-upload-item').last().attr('id', 'qq-upload-item-' + id);
			}).on('cancel', function (event, id, name) {
				delete app.upload.selected[name];
				if ($.isEmptyObject(app.upload.selected)) {
					$fineuploader.find('#qq-doupload').addClass('qq-hide');
				}
				app.upload.count--;
			}).on('progress', function (event, id, name, uploadedBytes, totalBytes) {

			}).on('upload', function (event, id, name) {
				//called just before a file begins to upload
			}).on('validate', function (event, data, button) {
				return typeof app.upload.selected[data.name] === 'undefined';
			});

			$fineuploader.find('#qq-doupload').bind('click', app.upload.trigger);
			function highlightProgressBar(item, cls, msg) {
				cls = cls || 'primary';
				cls = 'progress-bar-' + cls;
				var $item = $fineuploader.find('#qq-upload-item-' + item);
				$item.find('.progress-bar').addClass(cls).show();
				$item.find('.qq-upload-info').addClass('white').find('.qq-upload-file').append(msg);
				//$item.find('.qq-upload-cancel').show();
			}
		},
		upload: {
			selected: {},
			success: {},
			count: 0,
			uploaded: 0,
			trigger: function (e) {
				$fineuploader.find('#qq-doupload').addClass('qq-hide');
				$fineuploader.fineUploader('uploadStoredFiles');
			},
			reset: function () {
				app.upload.selected = {};
				app.upload.success = {};
				app.upload.count = 0;
				app.upload.uploaded = 0;
				//$fineuploader.fineUploader('reset');
			},
			completed: function(files) {
				app.upload.reset();
				if ($.isEmptyObject(files)) {
					return;
				}

				$fineuploader.find('#qq-doparse').removeClass('qq-hide').unbind('click').bind('click', function() {
					var codes = app.upload.getFileIds(files);
					if (codes.length) {
						redirect(config.parseUrl + '/?id=' + codes.join(','));
					}
				});
			},
			parse: function (files, force) {
				if ($.isEmptyObject(files)) {
					app.upload.reset();
					return;
				}

				var ids = app.upload.getFileIds(files);
				for (var i in ids) {
					app.upload.parseRequest(ids[i], force);
				}
			},
			parseRequest: function(id, force) {
				function wrapMessage(msg, cls) {
					return '<span class="'+cls+'">'+msg+'</span><br />';
				}

				app.upload.reset();
				post(config.uploadUrl, {parse: true, ids: [id], force: force}, function(response) {
					var $parseDom = $('#'+id);
					var $msg = $parseDom.find('.message');
					var cls = null;
					$msg.text('');

					if (response && response.success) {
						if (response.successes) {
							$.each(response.successes, function(i, msg) {
								$msg.append(wrapMessage(msg, 'green'));
								cls = 'alert-success';
							});
						}
						if (response.errors) {
							$.each(response.errors, function(i, msg) {
								$msg.append(wrapMessage(msg, 'red'));
							});
							cls = cls ? 'alert-warning' : 'alert-danger';
						}
					} else {
						var msg = response.message || 'A fatal error occured while parsing files!';
						$msg.append(wrapMessage(msg, 'red'));
						cls = 'alert-danger';
					}
					$parseDom.removeClass('alert-info').addClass(cls);
					$parseDom.find('.fa-spin').remove();
				});
			},
			getFileIds: function(files) {
				var codes = [];
				for (var i in files) {
					var code = files[i].id;
					if (code) {
						codes.push(code);
					}
				}
				return codes;
			}
		},
		edit: {
			save: function () {
				var $editor = $('.fileditor');
				var url = $editor.data('url');
				var file_contents = $editor.find('textarea').val();
				var file_man = $editor.data('man');
				if (!url || !file_man || !file_contents) {
					return;
				}

				post(url, {file_content: file_contents, file_man: file_man}, function(response) {
					if (response && response.success) {
						redirect(response.url);
					} else {
						bs_alert({type: 'danger', message: response.message || "An Error Occured while saving file"}); return;
					}
				});
			}
		}
	};

	window.RmdParser = app;
	$(document).ready(function () {
		window.RmdParser.initialize();
	});
})(jQuery);