<?php

define('RMDP_DIR', dirname(__FILE__));

define('RMDP_CONFIG_DIR', RMDP_DIR . '/config');

define('RMDP_FUNC_DIR', RMDP_DIR . '/functions');

define('RMDP_APP_DIR', RMDP_DIR . '/application');

require_once RMDP_FUNC_DIR . '/functions.php';

$config = array();

require_once RMDP_CONFIG_DIR . '/config.php';

if ($devenv = getenv('RMDP_CONFIG_ENV')) {
    $file = RMDP_CONFIG_DIR . '/' . $devenv . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
}

if (($check_setup = rmdparser_check_requirements($config)) !== true) {
    show_error($check_setup);
}

if (file_exists(RMDP_DIR . '/vendor/autoload.php')) {
	require_once RMDP_DIR . '/vendor/autoload.php';
}
$autoloader = require_once RMDP_DIR . '/lib/RmdParser/Autoloader.php';

RmdParser_Config::initialize($config);

error_reporting(-1);

if (($environment = RmdParser_Config::get('environment'))) {
    switch ($environment) {
        case 'dev':
            error_reporting(E_ALL);
        break;
        
        case 'test':
        case 'prod':
            error_reporting(0);
        break;
        default:
        show_error('The application environment is not set correctly in $config[environment].');
    }
}

if (($log_file = RmdParser_Config::get('error_log_file'))) {
	if (!file_exists($log_file) && !create_file($log_file)) {
		show_error("Unable to create custom log file {$log_file}");
	}
    ini_set('error_log', $log_file);
}

date_default_timezone_set(RmdParser_Config::get('timezone'));

mb_internal_encoding('UTF-8');
