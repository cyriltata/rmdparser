<?php

class SuperController extends RmdParser_Controller {

	public function __construct() {
		parent::__construct();
		if (!$this->isAuthenticatedSession()) {
			redirect('login?redirect=' . urlencode($this->router->current_url));
		}

		if (!$this->getUser()->is_admin) {
			throw new Exception("You are not authorized to see this section");
		}
	}

	public function indexAction() {
		redirect('super/users');
	}

	public function filesAction($user_code = '') {
		$results = RmdParser_Model::getAllUploads($user_code);
		$files = array();
		if ($results) {
			foreach ($results as $data) {
				$upload_id = $data['upload_id'];
				if (!isset($files[$upload_id])) {
					$files[$upload_id] = $data;
					$files[$upload_id]['files'] = array();
				}
				$data['file_exists'] = $this->fileExists(RmdParser_Model::getUserDataDir($data['username']), $data['file_filename']);
				$files[$upload_id]['files'][] = $data;
			}
		}

		$this->setVar('files', $files);
		$this->renderView('admin/super/files');
	}

	public function usersAction() {
		$users = RmdParser_Model::classMaps('RmdParser_Model_User', $this->db->find('users'));
		$this->renderView('admin/super/users', array('users' => $users));
	}

	private function getFileCounts() {
		$user = $this->getVars('user');
		return array(
			'uploads' => $this->db->count('uploads', array('user_id' => $user->id)),
			'completed' => $this->db->count('files', array('user_id' => $user->id, 'state' => RmdParser_Model_File::STATE_COMPLETED)),
			'failed' => $this->db->count('files', array('user_id' => $user->id, 'state' => RmdParser_Model_File::STATE_FAILED)),
			'partial' => $this->db->count('files', array('user_id' => $user->id, 'state' => RmdParser_Model_File::STATE_NEW)),
		);
	}

	private function fileExists($dir, $filename) {
		$filepath = $dir . '/' . $filename;
		return file_exists($filepath);
	}

}
