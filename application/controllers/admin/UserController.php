<?php

class UserController extends RmdParser_Controller {

	public function __construct() {
		parent::__construct();
		if (!$this->isAuthenticatedSession()) {
			redirect('login?redirect=' . urlencode($this->router->current_url));
		}
	}

	public function indexAction() {
		$this->setVar('counts', $this->getFileCounts());
		$this->renderView('admin/user');
	}

	public function filesAction() {
		$user = $this->getVars('user');
		$results = $user->getFiles();
		$files = array();
		if ($results) {
			foreach ($results as $data) {
				$upload_id = $data['upload_id'];
				if (!isset($files[$upload_id])) {
					$files[$upload_id] = $data;
					$files[$upload_id]['files'] = array();
				}
				$data['file_exists'] = $this->fileExists($data['file_filename']);
				$files[$upload_id]['files'][] = $data;
			}
		}

		$this->setVar('files', $files);
		$this->renderView('admin/user-files');
	}

	private function getFileCounts() {
		$user = $this->getVars('user');
		return array(
			'uploads' => $this->db->count('uploads', array('user_id' => $user->id)),
			'completed' => $this->db->count('files', array('user_id' => $user->id, 'state' => RmdParser_Model_File::STATE_COMPLETED)),
			'failed' => $this->db->count('files', array('user_id' => $user->id, 'state' => RmdParser_Model_File::STATE_FAILED)),
			'partial' => $this->db->count('files', array('user_id' => $user->id, 'state' => RmdParser_Model_File::STATE_NEW)),
		);
	}

	private function fileExists($filename) {
		$user = $this->getVars('user');
		$filepath = $user->getDataDir() . '/' . $filename;
		return file_exists($filepath);
	}

}
