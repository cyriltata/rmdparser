<?php

class IndexController extends RmdParser_Controller {

	public function __construct() {
		parent::__construct();
		if (!$this->isAuthenticatedSession()) {
			redirect('login?redirect=' . urlencode($this->router->current_url));
		}
	}

	public function indexAction() {
		redirect('admin/user');
	}

	public function uploadAction() {
		if ($this->request->isGetRequest()) {
			return $this->renderView('admin/upload');
		}

		if (!$this->request->isPostRequest() || !$this->request->isXMLHttpRequest()) {
			header("HTTP/1.0 405 Method Not Allowed");
			exit;
		}

		if ($this->request->bool('parse')) {
			return $this->parseFiles($this->request->arr('ids'));
		}

		$upload_params = array(
			'sizeLimit' => RmdParser_Config::get('max_file_upload_size'),
			'inputName' => 'qqfile',
			'chunksFolder' => null,
		);

		$uploader = new RmdParser_Uploader($upload_params);
		$upload = new RmdParser_Model_Upload();

		$user = RmdParser_Model_User::getFromSession();
		$directory = $user->getDataDir();
		$original_filename = $uploader->getName();
		$filename = $upload->generateNewFilename($original_filename);

		$result = $uploader->handleUpload($directory, $filename);
		$result["file"] = $uploader->getUploadName();
		if (!empty($result['success'])) {
			// save upload entry in db
			$code = $upload->createCode();
			$id = $upload->create(array(
				'code' => $code,
				'user_id' => $user->id,
				'filename' => $filename,
				'created' => time(),
				'original_filename' => $original_filename,
					), array('str', 'int', 'str', 'int', 'str'));
			if (!$id) {
				$result['success'] = false;
				$result['error'] = "File uploaded successfully but could not be saved in db";
			}
			$result['id'] = $code;
		}

		header("Content-Type: text/plain");
		echo json_encode($result);
		exit;
	}

	public function parseAction() {
		$id = $this->request->str('id');
		$force = $this->request->str('force') === 'true';
		if (!$id) {
			show_error('Invalid Request', 400);
		}

		$ids = explode(',', $id);
		$files = $js_files = array();
		foreach ($ids as $code) {
			$file = new RmdParser_Model_Upload($code);
			if (!$this->getUser()->canModify($file)) {
				show_error("You do not have permissions to modify this file");
			}
			if ($file->id) {
				$js_files[] = array('id' => $file->code);
				$files[] = $file;
			}
		}

		$this->setVar('force', $force);
		$this->setVar('files', $files);
		$this->setVar('uploads', $js_files);
		$this->renderView('admin/parse');
	}

	public function editAction($file = '') {
		if (!$file) {
			show_error("File parameter not not found", 404);
		}

		$user = $this->getUser();
		$file = new RmdParser_Model_Upload($file);
		if (!$file->id || !$user) {
			show_error("File not found", 404);
		}

		if (!$user->canModify($file)) {
			show_error("You do not have permissions to modify this file");
		}

		if ($this->request->isGetRequest()) {
			return $this->renderView('admin/edit', array(
				'file' => $file,
				'contents' => file_get_contents($file->getPath()),
			));
		} elseif ($this->request->isXMLHttpRequest()) {
			$file_content = $this->request->str('file_content');
			$file_user = $this->request->int('file_man');

			if (!$file_content || !$file_user || $file_user != $file->user_id) {
				return $this->renderJSON(array('success' => false, 'message' => 'Invalid Request'));
			}

			$filename = $file->getPath();
			if (file_put_contents($filename, $file_content)) {
				return $this->renderJSON(array('success' => true, 'url' => admin_url('parse/?force=true&id=' . $file->code, false)));
			} else {
				return $this->renderJSON(array('success' => false, 'message' => 'An error occured writing to file'));
			}
		}
		show_error("Invalid Request", 500);
	}

	private function parseFiles(array $ids) {
		if (!$ids || !is_array($ids)) {
			$this->renderJSON(array(
				'success' => false,
				'message' => 'No IDs found',
			));
		}

		try {
			$data = array('success' => true, 'errors' => null, 'successes' => null);
			foreach ($ids as $id) {
				$upload = new RmdParser_Model_Upload($id);
				$parsed = $upload->parse($this->request->bool('force'));
				if ($parsed !== true) {
					$data['errors'] = $upload->getParseErrors();
				}
				$data['successes'] = $upload->getParseSuccesses();
			}
		} catch (RmdParser_Exception $e) {
			$data = array('success' => false, 'message' => $e->getMessage());
		}
		$this->renderJSON($data);
	}

}
