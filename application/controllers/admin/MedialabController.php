<?php

class MedialabController extends RmdParser_Controller {

	public function __construct() {
		parent::__construct();
		if (!$this->isAuthenticatedSession()) {
			redirect('login?redirect=' . urlencode($this->router->current_url));
		}
	}

	public function indexAction() {
		$this->htmlQuestionaireAction();
	}

	public function htmlQuestionaireAction() {
		if ($this->request->isGetRequest() || !($items = $this->uploadExcelFile($this->request->str('postfix_name')))) {
			return $this->renderView('admin/medialab-createquestionaire');
		}

		// Create HTML file
		try {
			$html = '<table class="medialab-item-table"><tr>';
			$td_class = 'col-md-' . (12 / count($items['data']));
			foreach ($items['data'] as $column_data) {
				$html .= '<td class="'.$td_class.'" valign="top">';
				foreach ($column_data as $properties) {
					/* @var HTMLItem $item */
					$item = HTMLItem::make($properties['type'], $properties);
					$html .= $item->render();
				}
				$html .= '</td>';
			}
			$html = '<div class="col-md-12">'.$html.'</div>';
			$html .= '</tr></table>';

			$template = str_replace('%{ITEMS_HTML}', $html, file_get_contents(RMDP_DIR . '/htdocs/files/medialab.html'));
			$fname = date('d-m-y-H_i') . '-' . md5($template);
			$filename = RMDP_DIR . '/htdocs/files/' . $fname . '.html';
			if (!file_put_contents($filename, $template)) {
				throw new Exception("Unable to save HTML file. Make sure destination {$filename} is writable");
			}

			$download_link = admin_url('medialab/download-questionaire/' . $fname, false);
			$preview_link = site_url('files/' . basename($filename), false);
			$this->setSuccess('HTML file and associated created! <a href="' . $download_link . '" class="btn btn-default"><i class="fa fa-download"></i> Download Here</a> or <a href="' . $preview_link . '" target="_blank" class="btn btn-default"><i class="fa fa-eye"></i> Preview Here</a>');
		} catch (Exception $e) {
			$this->setError($e->getMessage());
		}
		$this->renderView('admin/medialab-createquestionaire');
	}

	public function downloadQuestionaireAction($filename) {
		$file = RMDP_DIR . '/htdocs/files/' . $filename . '.html';
		try {
			if (!file_exists($file)) {
				throw new Exception("File $filename not found");
			}
			$zipfile = $this->zipDownloadContent($file, $filename);
			$file_name = basename($zipfile);
			header("Content-Type: application/zip");
			header("Content-Disposition: attachment; filename=$file_name");
			header("Content-Length: " . filesize($zipfile));

			readfile($zipfile);
			unlink($zipfile);
			exit;
		} catch (Exception $ex) {
			log_exception($ex);
			$this->setError("An error occured zipping up files: " . $ex->getMessage());
		}
		$this->renderView('admin/medialab-createquestionaire');
	}

	protected function zipDownloadContent($file, $filename) {
		$zip = new ZipArchive();
		$zipfile = RMDP_DIR . '/htdocs/files/' . $filename . '.zip';
		$zip->open($zipfile, ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$zip->addFile($file, 'html/medialab.html');
		// Create recursive directory iterator
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(RMDP_DIR . '/htdocs/files/assets'), RecursiveIteratorIterator::LEAVES_ONLY);
		foreach ($files as $name => $file) {
			// Skip directories (they would be added automatically)
			if (!$file->isDir()) {
				// Get real and relative path for current file
				$filePath = $file->getRealPath();
				$relativePath = 'html/assets/' . basename($filePath);
				// Add current file to archive
				$zip->addFile($filePath, $relativePath);
			}
		}

		$zip->close();
		return $zipfile;
	}

	protected function uploadExcelFile($postfix_name = false) {
		if (empty($_FILES['excel_sheet']) || !empty($_FILES['excel_sheet']['error'])) {
			$this->setError("Excel sheet not uploaded");
			return false;
		}

		if ($_FILES['excel_sheet']['size'] > 5242880) {
			$this->setError("You can only upload a maximum of 5 MB");
			return false;
		}

		$items = medialab_parse_excel_file($_FILES['excel_sheet']['tmp_name'], $postfix_name);
		if (!$items['success']) {
			$this->setError($items['message']);
			return false;
		}
		return $items;
	}

}
