<?php


class PartnershipstudyController extends RmdParser_Controller {

	public function __construct() {
		parent::__construct();
		if (!$this->isAuthenticatedSession()) {
			redirect('login?redirect=' . urlencode($this->router->current_url));
		}
	}

	public function emailTemplateAction() {
		$file = RmdParser_Model_Partnershipstudy::EMAIL_TEMPLATE_BODY_FILE;
		if (!file_exists($file) && !file_put_contents($file, ' ')) {
			show_error('Unable to create file to store email body. Maybe not enough permissions to write on file system', null, 'File Error');
		}
		$file_subject = RmdParser_Model_Partnershipstudy::EMAIL_TEMPLATE_SUBJECT_FILE;
		if (!file_exists($file_subject) && !file_put_contents($file_subject, ' ')) {
			show_error('Unable to create file to store email subject. Maybe not enough permissions to write on file system', null, 'File Error');
		}

		if ($this->request->isPostRequest()) {
			if (file_put_contents($file, $_POST['email']) && file_put_contents($file_subject, $_POST['subject'])) {
				$this->setFlashMessage('Email template saved');
				redirect('admin/partnershipstudy/email-template');
			}
		}

		return $this->renderView('admin/partnershipstudy/edit', array(
			'file' => $file,
			'contents' => file_get_contents($file),
			'subject' => file_get_contents($file_subject),
		));
	}

	public function parseExcelAction() {
		if ($this->request->isGetRequest() || !($items = $this->uploadExcelFile())) {
			return $this->renderView('admin/partnershipstudy/upload');
		}

		if ($data = $items['data']) {
			$partnershipStudy = new RmdParser_Model_Partnershipstudy();
			$saved = $partnershipStudy->saveParticipantsData($data);
			if (!$saved) {
				$this->setError('There was an error inserting values in the database.');
			} else {
				$this->setFlashMessage('Participants data saved', 'success');
				redirect('admin/partnershipstudy/participants');
			}
		}

		return $this->renderView('admin/partnershipstudy/upload');
	}

	protected function uploadExcelFile() {
		if (empty($_FILES['excel_sheet']) || !empty($_FILES['excel_sheet']['error'])) {
			$this->setError("Excel sheet not uploaded");
			return false;
		}

		if ($_FILES['excel_sheet']['size'] > 5242880) {
			$this->setError("You can only upload a maximum of 5 MB");
			return false;
		}

		$items = partnershipstudy_parse_excel_file($_FILES['excel_sheet']['tmp_name']);
		if (!$items['success']) {
			$this->setError($items['message']);
			return false;
		}
		return $items;
	}

	public function participantsAction() {
		$partnershipStudy = new RmdParser_Model_Partnershipstudy();
		return $this->renderView('admin/partnershipstudy/participants', array(
			'participants' => $partnershipStudy->getParticipants(),
			'number' => 1,
		));
	}

	public function participantAction($id = 0, $action = '') {
		$id = (int)$id;
		$partnershipStudy = new RmdParser_Model_Partnershipstudy();
		if ($action === 'delete' && $id) {
			$this->db->delete('partnerstudy_participants', array('id' => (int)$id));
			$this->setFlashMessage('Entry has been deleted');
		} elseif ($action === 'test' && $id) {
			$sent = $partnershipStudy->sendInvitation($id);
			if ($sent) {
				$this->setFlashMessage('Invitation sent!');
			} else {
				$this->setFlashMessage('An error occured sending the invitation', 'error');
			}
		}
		redirect('admin/partnershipstudy/participants');
	}


}
