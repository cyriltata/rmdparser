<?php

class LoginController extends RmdParser_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->isAuthenticatedSession()) {
			redirect('admin/user');
		}
	}

	public function indexAction() {
		$redirect = $this->request->str('redirect');
		if (!$redirect) {
			$redirect = admin_url('user', false);
		}
		$this->setVar('redirect', $this->request->str('redirect'));

		if ($this->request->isGetRequest()) {
			return $this->renderView('login');
		}

		try {
			$auth = array(
				'username' => filter_var($this->request->str('user'), FILTER_SANITIZE_STRING),
				'password' => $this->request->str('passwd'),
			);
			RmdParser_Model_User::getInstance($auth['username'], $auth);
			redirect($redirect);
		} catch (RmdParser_Exception $e) {
			$this->setError($e->getMessage());
			log_exception($e);
		}

		$this->renderView('login', array('redirect' => $redirect));
	}

}
