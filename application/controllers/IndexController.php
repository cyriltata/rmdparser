<?php

class IndexController extends RmdParser_Controller {

	public function indexAction() {
		$msg = 'Welcome to RMD Parser Nothing to show here yet! ';
		$msg .= '<a href="' . site_url('admin', false) . '">Go to admin</a>';
		show_error($msg, null, 'Keep calm and markdown!');
	}

	public function fileAction($code = '', $upload = '') {
		$download = $this->request->bool('download', false);
		$is_upload = $upload === 'u';

		if (!$code) {
			show_error('Resource not found');
		}

		if ($is_upload) {
			$file = new RmdParser_Model_Upload($code);
		} else {
			$file = new RmdParser_Model_File($code);
		}

		if (!$file->id) {
			show_error('File not Found', 404);
		}

		$user = $this->db->findOne('users', array('id' => $file->user_id), array('username'));
		if (empty($user['username'])) {
			show_error('Invalid User File', 404);
		}

		$filename = $file->getPath(RmdParser_Model_User::getInstance($user['username'])->setProperties($user));
		if (!file_exists($filename)) {
			show_error('File does no longer exist');
		}

		$type = RmdParser_Model_File::getMime($filename);
		if ($download) {
			return $this->downloadFile($filename, $type);
		}

		//@todo Set cache headers
		header('Content-Type: ' . $type);
		echo file_get_contents($filename);
		exit;
	}

}
