<?php $this->renderView('common/header'); ?>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

        <?php $this->renderView('common/admin-nav'); ?>
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-user fa-fw"></i> <?php echo $user->getNames(); ?></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row col-lg-12">
            <div class="row col-md-8" style="margin-bottom: 25px;">
                <table class="table table-striped table-bordered">
                    <tbody>
                        <tr><td colspan="2"><b>INFO</b></td></tr>
                        <tr>
                            <td>First Name</td>
                            <td><?php echo $user->first_name; ?></td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td><?php echo $user->last_name; ?></td>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td><?php echo $user->username; ?></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><?php echo $user->email; ?></td>
                        </tr>
                        <tr>
                            <td>Created</td>
                            <td><?php echo date('d M Y H:i', $user->created); ?></td>
                        </tr>
                        <tr>
                            <td>Last Login</td>
                            <td><?php echo date('d M Y H:i', $user->last_login); ?></td>
                        </tr>
                         <tr>
                            <td>ID</td>
                            <td><?php echo $user->code; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <?php
                $this->renderView('common/ds-panel', array(
                    'title' => 'Uploaded',
                    'count' => $counts['uploads'],
                    'cls' => 'primary',
                    'link' => admin_url('user/files', false)));
                ?>
            </div>
            <div class="col-lg-3 col-md-6">
                <?php
                $this->renderView('common/ds-panel', array(
                    'title' => 'Parsed Formats',
                    'count' => $counts['completed'],
                    'cls' => 'success',
                    'link' => admin_url('user/files', false)));
                ?>
            </div>
            <div class="col-lg-3 col-md-6">
                <?php
                $this->renderView('common/ds-panel', array(
                    'title' => 'Partial Files',
                    'count' => $counts['partial'],
                    'cls' => 'warning',
                    'link' => admin_url('user/files', false)));
                ?>
            </div>
            <div class="col-lg-3 col-md-6">
                <?php
                $this->renderView('common/ds-panel', array(
                    'title' => 'Failed Files',
                    'count' => $counts['failed'],
                    'cls' => 'danger',
                    'link' => admin_url('user/files', false)));
                ?>
            </div>
        </div>
        <!-- /.row -->
        
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php $this->renderView('common/footer'); ?>