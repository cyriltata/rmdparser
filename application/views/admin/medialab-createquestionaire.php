<?php $this->renderView('common/header'); ?>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <?php $this->renderView('common/admin-nav'); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Create MediaLab HTML Questionaire</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row col-md-12" id="bootstrap-alert-container">&nbsp;</div>
            <div class="row" style="margin-bottom: 25px;">
                <div class="col-md-6">
					<?php $this->renderView('common/alerts', array('success' => $success, 'errors' => $errors)); ?>
					<h4>Upload Excel File</h4>
					<form class="" method="post" action="<?php admin_url('medialab/html-questionaire'); ?>" enctype="multipart/form-data" role="form">
						<div class="form-group col-md-6" style="padding: 0px; display: block;">
							<input type="file" class="form-control" name="excel_sheet" placeholder="Enter email">
						</div>
						<div class="clearfix"></div>
						<div class="form-group">
							<div class="checkbox">
								<label><input type="checkbox" name="postfix_name" value="true" /> Use Item Names</label>
							</div>
							<!--
							<div class="checkbox">
								<label><input type="checkbox" name="add_video_frame" value="true" /> Include video frame</label>
							</div>
							-->
						</div>
						<button type="submit" class="btn btn-default"><i class="fa fa-upload"></i> Upload</button>
					</form>
				</div>
				<div class="col-md-6">
					<h4>Doc</h4>
					<p>
						<b>What is this?</b> This tool helps you create and HTML file that can be used a  <i>custom questionaire item type</i> in MediaLAB.
					</p>
					<p>Sample Excel <a href="<?php site_url('files/medialab.xlsx'); ?>">Download Here</a></p>
					<p> Use this <a href="https://exceljet.net/keyboard-shortcuts/start-a-new-line-in-the-same-cell" target="_blank">keyboard shortcut</a> to add new lines when defining choices</p>
					<p> HTML file is generated according to this <a href="http://www.empirisoft.com/medialab/help/single_vs__multiple_variables.htm" target="_blank">Media Lab Doc</a> </p>
					<p> If everything is successful, you can download a zip file contains a folder named <b>html</b>, which contains all the files you need to move to MediaLab</p>
				</div>
            </div>

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $this->renderView('common/footer'); ?>