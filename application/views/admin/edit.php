<?php $this->renderView('common/header'); ?>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <?php $this->renderView('common/admin-nav'); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" style="margin-bottom: 0px;">Edit [<?php echo $file->original_filename; ?>]
						<button class="btn btn-success pull-right" type="button" onclick="RmdParser.edit.save();"><i class="fa fa-save"></i> Save &amp; Parse</button></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row col-md-12" id="bootstrap-alert-container">&nbsp;</div>
			<div class="clearfix hidden-xs"></div>
            <div class="fileditor" data-man="<?php echo $file->user_id; ?>" data-url="<?php admin_url('edit/'.$file->code); ?>">
				<textarea class="col-md-12"><?php echo hescape($contents); ?></textarea>
			</div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $this->renderView('common/footer'); ?>