<?php $this->renderView('common/header'); ?>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <?php $this->renderView('common/admin-nav'); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Parsing</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row col-md-12" id="bootstrap-alert-container">&nbsp;</div>
            
            <div class="row">
                <div class="col-md-12">
                <?php if ($files) : ?>
                    <?php foreach ($files as $file) {
                        $this->renderView('common/parsed-file', array('file' => $file));
                    } ?>
                <?php else: ?>
                    <div class="alert alert-danger">
                        Nothing to Parse
                    </div>
                <?php endif; ?>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php if ($uploads): ?>
    <script type="text/javascript">
        RmdParserVars = {'uploads': <?php echo json_encode($uploads); ?>, 'force_parse': <?php echo json_encode($force);?>};
    </script>
    <?php endif; ?>
<?php $this->renderView('common/footer'); ?>