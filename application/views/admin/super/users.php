<?php $this->renderView('common/header'); ?>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

        <?php $this->renderView('common/admin-nav'); ?>
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-file fa-fw"></i> Users</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php if ($users) : ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Listing
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Names</th>
                                            <th>Username</th>
                                            <th>Created</th>
											<th>Last Login</th>
											<th>Staff/Student</th>
											<th>Files</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($users as $user):?>
                                        <tr>
                                            <td><?php echo $user->getNames(); ?></td>
											<td><?php echo $user->username; ?></td>
											<td><?php echo date('d-m-y H:i:s', $user->created); ?></td>
											<td><?php echo date('d-m-y H:i:s', $user->last_login); ?></td>
											<td><?php echo $user->is_staff ? 'staff' : 'student'; ?></td>
											<td><a href="<?php admin_url('super/files/' . $user->code); ?>" class="btn btn-default btn-outline btn-sm"><i class="fa fa-file"> View Files</i></a></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                <?php else: ?>
                    <div class="alert alert-danger">  No users </div>
                <?php endif; ?>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php $this->renderView('common/footer'); ?>