<?php $this->renderView('common/header'); ?>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <?php $this->renderView('common/admin-nav'); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Upload</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row col-md-12" id="bootstrap-alert-container">&nbsp;</div>
            <div class="row col-md-12" style="margin-bottom: 25px;">
                <div class="fineuploader"></div>
            </div>

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $this->renderView('common/footer'); ?>