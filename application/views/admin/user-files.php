<?php $this->renderView('common/header'); ?>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

        <?php $this->renderView('common/admin-nav'); ?>
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-file fa-fw"></i> Files, <?php echo $user->getNames(); ?></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php if ($files) : ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Listing
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Original Filename</th>
                                            <th>Uploaded</th>
                                            <th>Renditions</th>
											<th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($files as $file) : $file = (object) $file;?>
                                        <tr>
                                            <td><a href="<?php admin_url('edit/'.$file->upload_code); ?>"><?php echo $file->original_filename; ?></a></td>
                                            <td><?php echo date('d-m-Y H:i:s', $file->upload_created); ?></td>
                                            <td>
                                                <span><a href="<?php site_url('file/' . $file->upload_code . '/u'); ?>" class="type-icon rmd" target="_blank">rmd</a></span>
                                                <?php foreach ($file->files as $f): $f = (object) $f; ?>
													<?php if ($f->file_exists): ?>
														 <span><a href="<?php site_url('file/' . $f->file_code); ?>" class="type-icon <?php echo $f->type; ?>" target="_blank"><?php echo $f->type; ?></a></span>
													<?php endif; ?>
												<?php endforeach; ?>
                                            </td>
											<td>
												<a href="<?php admin_url('parse?force=true&id='.$file->upload_code); ?>" class="btn-default btn btn-sm">Re-parse <i class="fa fa-history"></i></a>
											</td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                <?php else: ?>
                    <div class="alert alert-danger">
                        Nothing to files uploaded. <a href="<?php admin_url('upload'); ?>">Upload.</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php $this->renderView('common/footer'); ?>