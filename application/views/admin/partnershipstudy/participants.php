<?php $this->renderView('common/header'); ?>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <?php $this->renderView('common/admin-nav'); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Participants List</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row col-md-12" id="bootstrap-alert-container">&nbsp;</div>
            <div class="row" style="margin-bottom: 25px;">
                <div class="col-md-12">
					<?php $this->renderView('common/alerts', array('success' => $success, 'errors' => $errors)); ?>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Participant ID</th>
									<th>Datum</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Study</th>
									<th>Invited</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php while($row = $participants->fetch(PDO::FETCH_ASSOC)): ?>
								<tr>
									<td><?= $number++ ?></td>
									<td><?= $row['id'] ?></td>
									<td><?= $row['init_date'] ?></td>
									<td><?= $row['first_name'] ?></td>
									<td><?= $row['last_name'] ?></td>
									<td><?= $row['email'] ?></td>
									<td><?= $row['study'] ?></td>
									<td><?= $row['invitation_sent'] ? 'YES' : 'NO' ?></td>
									<td>
										<a href="<?php admin_url('partnershipstudy/participant/' . $row['id'] . '/test'); ?>" class="btn btn-default"> Test </a>
										<a href="<?php admin_url('partnershipstudy/participant/' . $row['id'] . '/delete'); ?>" class="btn btn-default btn-danger" onclick="return confirm('Are you sure you want to delete this entry');"> Delete </a>
									</td>
								</tr>
								<?php endwhile; ?>
							</tbody>
						</table>
					</div>
				</div>
            </div>

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $this->renderView('common/footer'); ?>