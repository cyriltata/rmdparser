<?php $this->renderView('common/header'); ?>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <?php $this->renderView('common/admin-nav'); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Upload Particpants Excel</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row col-md-12" id="bootstrap-alert-container">&nbsp;</div>
            <div class="row" style="margin-bottom: 25px;">
                <div class="col-md-6">
					<?php $this->renderView('common/alerts', array('success' => $success, 'errors' => $errors)); ?>
					<h4>Upload Excel File</h4>
					<form class="" method="post" action="<?php admin_url('partnershipstudy/parse-excel'); ?>" enctype="multipart/form-data" role="form">
						<div class="form-group col-md-6" style="padding: 0px; display: block;">
							<input type="file" class="form-control" name="excel_sheet" placeholder="Enter email">
						</div>
						<div class="clearfix"></div>
						<button type="submit" class="btn btn-default"><i class="fa fa-upload"></i> Upload</button>
					</form>
				</div>
				<div class="col-md-6">
					<h4>Compulsory columns in Excelsheet</h4>
					<ul>
						<li>Teilnehmernummer</li>
						<li>Datum</li>
						<li>Nachname</li>
						<li>Vorname</li>
						<li>Email</li>
						<li>Studie</li>
						<li>Gender</li>
					</ul>
				</div>
            </div>

            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $this->renderView('common/footer'); ?>