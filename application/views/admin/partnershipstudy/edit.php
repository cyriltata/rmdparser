<?php $this->renderView('common/header'); ?>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <?php $this->renderView('common/admin-nav'); ?>
        </nav>

		<form action="" method="post">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" style="margin-bottom: 0px;">Edit Email Template <button class="btn btn-success pull-right" type="submit"><i class="fa fa-save"></i> Save </button></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row col-md-12" id="bootstrap-alert-container">
				<?php $this->renderView('common/alerts'); ?>
			</div>
			
			<label class="email-subject">Subject &nbsp; <input name="subject" value="<?php echo hescape($subject); ?>" class="" /></label>
			<div class="clearfix hidden-xs"></div>
			
			<label>Body</label>
            <div class="fileditor">
				<textarea class="col-md-12" name="email"><?php echo hescape($contents); ?></textarea>
			</div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
		</form>

    </div>
    <!-- /#wrapper -->

<?php $this->renderView('common/footer'); ?>