<div class="panel panel-<?php echo $cls; ?>">
    <div class="panel-heading">
        <div class="row">
            <div class="col-xs-3">
                <i class="fa fa-file fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
                <div class="huge"><?php echo $count; ?></div>
                <div><?php echo $title; ?></div>
            </div>
        </div>
    </div>
    <a href="<?php echo $link; ?>">
        <div class="panel-footer">
            <span class="pull-left">List All</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
        </div>
    </a>
</div>