<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?php site_url(); ?>">Rmd Parser 1.0</a>
</div>
<!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">
    <li>Logged in as <a href="<?php admin_url('user'); ?>" style="padding: 0; display: inline-block; min-height: initial;"><?php echo $user->getNames(); ?></a></li>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="<?php admin_url('user'); ?>"><i class="fa fa-user fa-fw"></i> <?php echo $user->getNames(); ?></a></li>
            <li class="divider"></li>
            <li><a href="<?php site_url('logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
			<li class="text-center" style="font-size: 11px;">RmdParser Menu</li>
            <li>
                <a href="<?php admin_url('upload'); ?>"><i class="fa fa-upload fa-fw"></i> Upload Files</a>
            </li>
            <li>
                <a href="<?php admin_url('user/files'); ?>"><i class="fa fa-file-archive-o fa-fw"></i> List My Files</a>
            </li>
			<li class="text-center" style="font-size: 11px;">MediaLAB Menu</li>
			<li>
                <a href="<?php admin_url('medialab/html-questionaire'); ?>"><i class="fa fa-list fa-fw"></i> Create HTML Questionaire</a>
            </li>
			<li class="text-center" style="font-size: 11px;">Partnership Study Menu</li>
			<li>
                <a href="<?php admin_url('partnershipstudy/email-template'); ?>"><i class="fa fa-envelope-o fa-fw"></i> E-mail Template</a>
            </li>
			<li>
                <a href="<?php admin_url('partnershipstudy/parse-excel'); ?>"><i class="fa fa-list fa-fw"></i> Upload Participants Excel</a>
            </li>
			<li>
                <a href="<?php admin_url('partnershipstudy/participants'); ?>"><i class="fa fa-users fa-fw"></i> Participants Listing</a>
            </li>
			<?php if ($user->is_admin): ?>
			<li class="text-center" style="font-size: 11px;">ADMIN</li>
			<li>
                <a href="<?php admin_url('super/files'); ?>"><i class="fa fa-table fa-fw"></i> List All Files</a>
            </li>
            <li>
                <a href="<?php admin_url('super/users'); ?>"><i class="fa fa-table fa-fw"></i> List All Users</a>
            </li>
			<?php endif; ?>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->