<?php /** @var $file RmdParser_Model_Upload */ ?>

<div class="alert alert-info parse-file" id="<?php echo $file->code; ?>">
    <i class="fa fa-file fa-3x pull-left"></i>
    <i class="fa fa-spinner fa-2x fa-spin pull-right"></i>
    <div class="inline-block pull-left">
        <span class="message">Converting <?php echo $file->original_filename; ?> ... </span><br />
        <b>File ID</b> <code><?php echo $file->code; ?></code>
    </div>
    <div class="clearfix"></div>
</div>
