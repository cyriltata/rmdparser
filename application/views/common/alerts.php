<?php if (!empty($success)) : ?>
<div class="alert alert-success alert-dismissable">
    <i class="fa fa-check pull-left"></i>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <ul class="pull-left list-unstyled">
        <?php foreach ($success as $msg): ?>
        <li><?php echo $msg; ?></li>
        <?php endforeach; ?>
    </ul>
    <div class="clearfix"></div>
</div>
<?php endif; ?>

<?php if (!empty($errors)) : ?>
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-exclamation-triangle pull-left"></i>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <ul class="pull-left list-unstyled">
        <?php foreach ($errors as $msg): ?>
        <li><?php echo $msg; ?></li>
        <?php endforeach; ?>
    </ul>
    <div class="clearfix"></div>
</div>
<?php endif; ?>

