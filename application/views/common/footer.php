
    <!-- jQuery -->
    <script src="<?php theme_url('js/jquery.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php theme_url('js/bootstrap.min.js'); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php theme_url('js/plugins/metisMenu/metisMenu.min.js'); ?>"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php asset_url('fineuploader/fineuploader.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php theme_url('js/sb-admin-2.js'); ?>"></script>
    <script type="text/javascript">
        <?php
            if (empty($js_params)) {
                $js_params = array();
            }
            $js_params = array_merge($js_params, array(
                'siteUrl' => site_url('', false),
                'uploadUrl' => site_url('admin/upload', false),
                'parseUrl' => site_url('admin/parse', false),
            ));
        ?>
        if (typeof RmdParserVars === 'undefined') {
            RmdParserVars = {};
        }
        RmdParserVars = $.extend(RmdParserVars, <?php echo json_encode($js_params); ?>);
    </script>
    <script src="<?php asset_url('app.js'); ?>"></script>

    <script type="text/rmdp" id="uploader-interface">
        <div class="qq-uploader">
			<div class="qq-upload-drop-area select-file-area">
				<div class="drag-drop-inside">
					<div class="span12 upload-hint">{dragZoneText}</div>
					<div class="clear"></div>
					<div class="qq-upload-button btn btn-primary"><div>{uploadButtonText}</div></div>
				</div>
			</div>
			<div class="clearfix"></div>
			<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>
			<div class="upload-list"></div>
            <div class="text-center qq-upbtn">
                <button class="btn btn-primary qq-hide" id="qq-doupload"><i class="fa fa-upload"></i> Upload Selected Files</button>
            </div>
    
            <div class="text-center qq-upbtn">
                <button class="btn btn-success qq-hide" id="qq-doparse"><i class="fa fa-file"></i> Parse successful uploads</button>
            </div>
		</div>
    </script>
    
    <script type="text/rmdp" id="uploader-interface-item">
        <div class="qq-upload-item">
            <div class="progress">
                <div class="data progress-bar progress-bar-striped active" role="progressbar"></div>
            </div>
            <div class="qq-upload-info">
                <span class="qq-upload-file pull-left"></span>
                <a class="qq-upload-cancel btn alert-danger pull-right red" href="#"><i class="fa fa-times"></i> {cancelButtonText}</a>
            </div>
            <span class="qq-upload-spinner qq-hide"></span>
            <span class="qq-upload-finished"></span>
            <span class="qq-upload-size qq-hide"></span>
            <a class="qq-upload-retry qq-hide" href="#">{retryButtonText}</a>
            <span class="qq-upload-status-text qq-hide">{statusText}</span>
        </div>
    </script>
    <script type="text/rmdp" id="bootstrap-alert-tpl">
        <div class="alert alert-%{type} alert-dismissable">
            <i class="fa %{icon} pull-left"></i>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <div class="pull-left list-unstyled">%{message}</div>
            <div class="clearfix"></div>
        </div>
    </script>
</body>

</html>
