<?php

abstract class HTMLItem {

	protected $id;
	protected $label;
	protected $name;
	protected $type;
	protected $value;
	protected $class = '';
	protected $choices = array();
	protected $require_choices = false;
	protected $postfix_name = false;
	protected $index = null;

	abstract public function render();

	public function __construct($properties = array()) {
		if ($properties) {
			$this->setProperties($properties);
		}
	}

	public function requiresChoices() {
		return $this->require_choices;
	}

	public function getName() {
		return $this->name;
	}

	public function setProperties($props) {
		foreach ($props as $property => $value) {
			if (property_exists($this, $property)) {
				$this->{$property} = $value;
			}
		}
	}

	protected function getHTML($template) {
		if ($this->postfix_name) {
			$this->name = '<ml.varname>_' . $this->name;
		} else {
			$this->name = '<ml.varname>_' . str_pad(($this->index+1), 2, '0', STR_PAD_LEFT);
		}
		$vars = get_object_vars($this);
		unset($vars['choices'], $vars['require_choices'], $vars['index'], $vars['postfix_name']);
		foreach ($vars as $key => $value) {
			$template = str_replace('%{'.$key.'}', $value, $template);
		}
		return $template;
	}
	/**
	 * 
	 * @param string $type
	 * @param array $properties
	 * @throws HTMLItem_Exception
	 *
	 * @return HTMLItem
	 */
	public static function make($type, $properties = array()) {
		$class = 'HTMLItem_' . ucwords(strtolower($type));
		if (!class_exists($class, true)) {
			throw new HTMLItem_Exception("Item of type '$type' is not supported");
		}
		return new $class($properties);
	}
	
}

class HTMLItem_Text extends HTMLItem {

	public function render() {
		$tpl =  '
		<div class="form-group %{class}">
			<div class="form-label">%{label}</div>
			<input type="text" class="form-control" id="%{id}" name="%{name}" value="%{value}">
		</div>';
		return $this->getHTML($tpl);
	}
}

class HTMLItem_Email extends HTMLItem_Text {

	public function render() {
		$tpl =  '
		<div class="form-group %{class}">
			<div class="form-label">%{label}</div>
			<input type="email" class="form-control" id="%{id}" name="%{name}" value="%{value}">
		</div>';
		return $this->getHTML($tpl);
	}
}

class HTMLItem_Bigtext extends HTMLItem {

	public function render() {
		$tpl =  '
		<div class="form-group %{class}">
			<div class="form-label">%{label}</div>
			<textarea class="form-control" rows="3" id="%{id}" name="%{name}">%{value}</textarea>
		</div>';
		return $this->getHTML($tpl);
	}
}

class HTMLItem_Single extends HTMLItem {

	protected $require_choices = true;

	protected static $label_markers = array('ll', 'rl');
	public function render() {
		$tpl =  '
		<div class="form-group">
			<div class="form-label">%{label}</div>
		';
		foreach ($this->choices as $choice_value => $choice_label) {
			$checked = $choice_value == $this->value ? 'checked="checked"' : '';
			if ($choice_value && in_array($choice_value, self::$label_markers)) {
				$control = '<label class="%{class} '.$choice_value.'">'.$choice_label.'</label>';
			} else {
				$control = '<label class="%{class}"><input type="radio" name="%{name}" value="'.htmlentities($choice_value).'" '.$checked.' />'.$choice_label.'</label>';
			}
			if (!strstr($this->class, 'inline')) {
				$control = '<div class="radio">' . $control . '</div>';
			}
			$tpl .= $control;
		}
		$tpl .= '
		</div>';
		return $this->getHTML($tpl);
	}
}

class HTMLItem_Multiple extends HTMLItem {
	protected $require_choices = true;

	public function render() {
		$tpl =  '
		<div class="form-group">
			<div class="form-label">%{label}</div>
		';
		$count_choices = count($this->choices);
		foreach ($this->choices as $choice_value => $choice_label) {
			$checked = $choice_value == $this->value ? 'checked="checked"' : '';
			if ($this->postfix_name) {
				$name = '<ml.varname>_' . $this->name . '_' . $count_choices;
			} else {
				$name = '<ml.varname>_' . str_pad(($this->index+1-$count_choices), 2, '0', STR_PAD_LEFT);
			}
			$control = '<label class="%{class}"><input type="checkbox" name="'.$name.'" value="'.htmlentities($choice_value).'" '.$checked.' />'.$choice_label.'</label>';
			if (!strstr($this->class, 'inline')) {
				$control = '<div class="checkbox">' . $control . '</div>';
			}
			$tpl .= $control;
			$count_choices--;
		}
		$tpl .= '
		</div>';
		return $this->getHTML($tpl);
	}
}

class HTMLItem_Select extends HTMLItem {
	protected $require_choices = true;

	public function render() {
		$tpl =  '
		<div class="form-group">
			<div class="form-label">%{label}</div>
			<select class="form-control %{class}" name="%{name}">
		';
		foreach ($this->choices as $choice_value => $choice_label) {
			$selected = $choice_value == $this->value ? 'selected="selected"' : '';
			$tpl .= '<option value="'.htmlentities($choice_value).'" '.$selected.'>'.$choice_label.'</option>';
		}
		$tpl .= '
		</select></div>';
		return $this->getHTML($tpl);
	}
}

class HTMLItem_Note extends HTMLItem {
	public function render() {
		return '
			<div class="form-group form-label">'.$this->label.'</div>
		';
		
	}
}

class HTMLItem_Exception extends Exception {}

