<?php

class RmdParser_Autoloader {

	private static $loader = null;
	private $class_path = null;

	/**
	 * Register this class instance as an autoloader
	 * @throws RuntimeException
	 */
	public function register() {
		$this->class_path = RMDP_DIR . '/lib';
		if (!spl_autoload_register(array($this, 'loadClass'), true)) {
			throw new RuntimeException(__CLASS__ . '::register() failed.');
		}
	}

	/**
	 * unregister class instance as an autoloader
	 */
	public function unregister() {
		spl_autoload_unregister(array($this, 'loadClass'));
	}

	/**
	 * Loads the given class
	 *
	 * @param string $class The name of the class
	 * @return bool Returns TRUE if class is loaded
	 * @throws RuntimeException
	 */
	public function loadClass($class) {
		if (class_exists($class, false)) {
			return true;
		}

		if ($file = $this->findFile($class)) {
			include $file;
			if (!class_exists($class)) {
				$message = sprintf("Autloader expected class %s to be defined in file %s. The class was found but the file was not in it", $class, $file);
				throw new RuntimeException($message);
			}
			return true;
		}

		return false;
	}

	/**
	 * Finds the path associated to the class name
	 *
	 * @param string $class
	 * @return string|null
	 */
	private function findFile($class) {
		$class = str_replace('_', '/', $class);
		$file = $this->class_path . '/' . $class . '.php';
		if (file_exists($file)) {
			return $file;
		}
		return false;
	}

	public static function getLoader() {
		if (self::$loader === null) {
			$loader = new self();
			$loader->register();
			self::$loader = $loader;
		}
		return self::$loader;
	}

}

return RmdParser_Autoloader::getLoader();
