<?php

class RmdParser_OpenCPU {

	protected $baseUrl = 'https://public.opencpu.org';
	protected $libUri = '/ocpu/library';
	protected $cache = array();
	protected $last_message = null;

	public function __construct($baseUrl = '') {
		$this->setBaseUrl($baseUrl);
	}

	public function setBaseUrl($baseUrl) {
		if ($baseUrl) {
			$baseUrl = rtrim($baseUrl, "/");
			$this->baseUrl = $baseUrl;
		}
	}

	public function getBaseUrl() {
		return $this->baseUrl;
	}

	public function setLibUrl($libUri) {
		$libUri = trim($libUri, "/");
		$this->libUri = '/' . $libUri;
	}

	public function getLibUrl() {
		return $this->libUri;
	}

	public function getLastMessage() {
		return $this->last_message;
	}

	private function call($uri = '', $params = array(), $method = RmdParser_CURL::HTTP_METHOD_GET) {
		$cachekey = md5(serialize(func_get_args()));
		if (isset($this->cache[$cachekey])) {
			return $this->cache[$cachekey];
		}

		if ($uri) {
			$uri = "/" . ltrim($uri, "/"); // just to be fucking sure
		}
		$url = $this->baseUrl . $this->libUri . $uri;

		$results = RmdParser_CURL::HttpRequest($url, $params, $method, array(), $info);
		if ($info['http_code'] < 200 || $info['http_code'] > 300) {
			if (!$results) {
				$results = "OpenCPU server '{$this->baseUrl}' could not be contacted";
			}
			log_message('debug', "OpenCPU: $results");
			throw new Exception($results, $info['http_code']);
		}
		// $headers = $info[RmdParser_CURL::RESPONSE_HEADERS];
		// print_r($headers); exit;
		return $results;
	}

	/**
	 * Send a POST request to OpenCPU
	 *
	 * @param string $uri A uri that is relative to openCPU's library entry point for example '/markdown/R/render'
	 * @param array $params An array of parameters to pass
	 * @return mixed Returns the response from openCPU
	 */
	public function post($uri = '', $params = array()) {
		return $this->call($uri, $params, RmdParser_CURL::HTTP_METHOD_POST);
	}

	/**
	 * Send a GET request to OpenCPU
	 *
	 * @param string $uri A uri that is relative to openCPU's library entry point for example '/markdown/R/render'
	 * @param array $params An array of parameters to pass
	 * @return mixed Returns the response from openCPU
	 */
	public function get($uri = '', $params = array()) {
		return $this->call($uri, $params, RmdParser_CURL::HTTP_METHOD_GET);
	}

	/**
	 * Just some special method to use rmarkdown to convert files into various formats
	 * A shortcut that was needed
	 * @see http://cran.r-project.org/web/packages/rmarkdown/index.html
	 *
	 * @param string $filename Full path to file that is to be converted
	 * @param string $output The output method to call in rmarkdown R package
	 * @return returns TRUE if conversion was success full or FALSE otherwise
	 */
	public function rmarkdownRender($filename, $output = 'html_document') {
		$uri = '/rmarkdown/R/render';
		$params = array(
			'output_format' => '"' . $output . '"',
			'input' => RmdParser_CURL::getPostFileParam($filename, basename($filename)),
		);

		try {
			$response = $this->post($uri, $params);
			$files = $this->getFilesFromResponse($response);
			return $files;
		} catch (Exception $e) {
			$this->last_message = $e->getMessage();
			return false;
		}
	}

	/**
	 * Execute a piece of code against open cpu
	 *
	 * @param string $code Each code line should be separated by a newline characted
	 * @param string $return_format String like 'json'
	 * @return mixed Returns response from open CPU
	 */
	public function exec($code, $return_format = 'json') {
		$params = array('x' => '{ 
            (function() {
                ' . $code . '
            })()
        }');

		$uri = '/base/R/identity/' . $return_format;
		return $this->post($uri, $params);
	}

	/**
	 * Get an array of files present in current session
	 *
	 * @param string $result Output from post() or get() methods
	 * @return array
	 */
	public function getFilesFromResponse($result) {
		$files = array();
		$result = explode("\n", $result);
		log_message('debug', $result);
		log_message('debug', array_map(array($this, 'getResponsePath'), $result));

		foreach ($result as $path) {
			if (!$path || strpos($path, '/files/') === false) {
				continue;
			}
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			if (!$ext) {
				$ext = pathinfo($path, PATHINFO_BASENAME);
			}
			$files[$ext] = $this->getResponsePath($path);
		}
		return $files;
	}

	public function getResponsePath($path) {
		return $this->baseUrl . $path;
	}

}
