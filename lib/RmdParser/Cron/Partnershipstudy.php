<?php

class RmdParser_Cron_Partnershipstudy {

	/**
	 * 
	 * @var DB
	 */
	protected $db;

	/**
	 *
	 * @var PHPMailer[]
	 */
	protected $connections = array();

	/**
	 *
	 * @var int[]
	 */
	protected $failures = array();
	protected $restInterval = 20;
	protected static $logFile = null;

	public function __construct(RmdParser_Db $db) {
		$this->db = $db;
		$this->restInterval = RmdParser_Config::get('partnership_study_email.loop_rest_interval', $this->restInterval);

		$logFile = RmdParser_Config::get('partnership_study_email.log_file');
		if ($logFile) {
			if (!file_exists($logFile) && !create_file($logFile)) {
				show_error("Unable to create custom log file {$logFile}");
			}
			self::$logFile = $logFile;
		}
	}

	/**
	 * 
	 * @param int $account_id
	 * @return PDOStatement
	 */
	protected function getEmailsStatement() {
		$query = 'SELECT id, init_date, first_name, last_name, email, invitation_sent, study, gender_greet FROM partnerstudy_participants WHERE invitation_sent = 0 ORDER BY id ASC';
		return $this->db->rquery($query);
	}

	/**
	 *
	 * @param array $account
	 * @return PHPMailer
	 */
	protected function getSMTPConnection($account) {
		$account_id = $account['account_id'];
		if (!isset($this->connections[$account_id])) {
			$mail = new PHPMailer();
			$mail->SetLanguage("de", "/");

			$mail->isSMTP();
			$mail->SMTPAuth = true;
			$mail->SMTPKeepAlive = true;
			$mail->Mailer = "smtp";
			$mail->Host = $account['host'];
			$mail->Port = $account['port'];
			if ($account['tls']) {
				$mail->SMTPSecure = 'tls';
			} else {
				$mail->SMTPSecure = 'ssl';
			}
			$mail->Username = $account['username'];
			$mail->Password = $account['password'];

			$mail->setFrom($account['from'], $account['from_name']);
			$mail->AddReplyTo($account['from'], $account['from_name']);
			$mail->CharSet = "utf-8";
			$mail->WordWrap = 65;
			$mail->AllowEmpty = true;
			$mail->isHTML(true);

			if (is_array(RmdParser_Config::get('partnership_study_email.smtp_options'))) {
				$mail->SMTPOptions = array_merge($mail->SMTPOptions, RmdParser_Config::get('partnership_study_email.smtp_options'));
			}

			$this->connections[$account_id] = $mail;
		}
		return $this->connections[$account_id];
	}

	protected function closeSMTPConnection($account_id) {
		if (isset($this->connections[$account_id])) {
			unset($this->connections[$account_id]);
		}
	}

	protected function processQueue($account_id = null) {
		$emailContent = @file_get_contents(RmdParser_Model_Partnershipstudy::EMAIL_TEMPLATE_BODY_FILE);
		$emailSubject = @file_get_contents(RmdParser_Model_Partnershipstudy::EMAIL_TEMPLATE_SUBJECT_FILE);

		if (!$emailContent) {
			self::dbg('Email content not set');
			return false;
		}

		$account = (array) RmdParser_Config::get('partnership_study_email');
		$account['account_id'] = 'partnerstudy';

		$sentCount = 1;
		$emailsStatement = $this->getEmailsStatement($account['account_id']);
		while ($email = $emailsStatement->fetch(PDO::FETCH_ASSOC)) {
			$email['study_label'] = partnershipstudy_get_study_label($email['study']);
			$mailer = $this->getSMTPConnection($account);
			// check if smtp connection is lost and kill object
			if (!$mailer->getSMTPInstance()->connected()) {
				$this->closeSMTPConnection($account['account_id']);
				$mailer = $this->getSMTPConnection($account);
			}

			$emails = explode(';', $email['email']);
			$email['recipient'] = end($emails);
			$email['subject'] = trim($emailSubject);
			$email['message'] = partnershipstudy_email_template($emailContent, $email);

			if (!filter_var($email['recipient'], FILTER_VALIDATE_EMAIL) || !$emailSubject) {
				$this->registerFailure($email);
				continue;
			}

			$debugInfo = json_encode(array('id' => $email['id'], 'r' => $email['recipient']));

			$mailer->Subject = $email['subject'];
			$mailer->msgHTML($email['message']);
			$mailer->addAddress($email['recipient']);

			// Send mail
			try {
				$sent = $mailer->send();
				if ($sent) {
					$this->db->exec("UPDATE partnerstudy_participants SET invitation_sent = 1 WHERE id = " . (int) $email['id']);
					self::dbg("Send Success: - {$debugInfo}");
					$sentCount++;
				} else {
					throw new Exception($mailer->ErrorInfo);
				}
			} catch (Exception $e) {
				self::dbg("Send Failure: - " . $mailer->ErrorInfo . ".\n {$debugInfo}");
				$this->registerFailure($email);
				// reset php mailer object for this account if smtp sending failed. Probably some limits have been hit
				$this->closeSMTPConnection($account['account_id']);
				$mailer = $this->getSMTPConnection($account);
			}

			$mailer->clearAddresses();
			$mailer->clearAttachments();
			$mailer->clearAllRecipients();

			if ($sentCount % $this->restInterval == 0) {
				self::dbg('---- sleeping for 8 seconds -----');
				sleep(8);
			}


		}
		// close sql emails cursor after processing batch
		$emailsStatement->closeCursor();

		return true;
	}

	/**
	 * Register email send failure and/or remove expired emails
	 *
	 * @param array $email @array(id, subject, message, recipient, created, meta)
	 */
	protected function registerFailure($email) {
		$id = $email['id'];
		if (!isset($this->failures[$id])) {
			$this->failures[$id] = 0;
		}
		$this->failures[$id] ++;
		self::dbg('Invalid Email: ' . $email['email']);
	}

	public function run($account_id = null) {
		try {
			$this->processQueue($account_id);
		} catch (Exception $e) {
			// if connection disappeared - try to restore it
			$error_code = $e->getCode();
			if ($error_code != 1053 && $error_code != 2006 && $error_code != 2013 && $error_code != 2003) {
				throw $e;
			}

			self::dbg($e->getMessage() . "[" . $error_code . "]");

			self::dbg("Unable to connect. waiting 5 seconds before reconnect.");
			sleep(5);
		}
	}

	protected static function dbg($str) {
		if (!self::$logFile) {
			return;
		}

		$args = func_get_args();
		if (count($args) > 1) {
			$str = vsprintf(array_shift($args), $args);
		}

		error_log($str . PHP_EOL, 3, self::$logFile);
	}

}
