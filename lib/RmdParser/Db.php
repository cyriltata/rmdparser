<?php

class RmdParser_Db {

	/**
	 * @var PDO
	 */
	protected $pdo;

	/**
	 *
	 * @var array
	 */
	protected $types = array(
		// Interger types
		'int' => PDO::PARAM_INT,
		'integer' => PDO::PARAM_INT,
		// String Types
		'str' => PDO::PARAM_STR,
		'string' => PDO::PARAM_STR,
		// Boolean types
		'bool' => PDO::PARAM_BOOL,
		'boolean' => PDO::PARAM_BOOL,
		// NULL type
		'null' => PDO::PARAM_NULL,
	);

	/**
	 * Default data-type
	 *
	 * @var interger
	 */
	protected $default_type = PDO::PARAM_STR;

	/**
	 *
	 * @var RmdParser_Db[]
	 */
	protected static $instances = array();

	/**
	 * 
	 * @param string $instance
	 *
	 * @return RmdParser_Db
	 */
	public static function getInstance($instance = 'database') {
		if (!isset(self::$instances[$instance])) {
			self::$instances[$instance] = new self(RmdParser_Config::get($instance));
		}
		return self::$instances[$instance];
	}

	protected function __construct(array $params) {
		$options = array(
			'host' => $params['host'],
			'port' => $params['port'],
			'dbname' => $params['dbname'],
			'charset' => 'utf8',
		);

		$dsn = 'mysql:' . http_build_query($options, null, ';');
		$this->pdo = new PDO($dsn, $params['username'], $params['password'], array(
			PDO::ATTR_EMULATE_PREPARES => false,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		));
	}

	public function __destruct() {
		$this->pdo = null;
	}

	/**
	 * Find a set of records from db
	 *
	 * @param string $table_name
	 * @param string|aray $where
	 * @param array $params
	 * @return array
	 */
	public function find($table_name, $where = null, $params = array()) {
		$this->quoteCol($table_name);
		if (empty($params['cols'])) {
			$select = '*';
		} else {
			$select = implode(',', array_map(array($this, 'quoteCol'), $params['cols']));
		}

		$query = "SELECT {$select} FROM {$table_name}";
		if ($where && is_string($where)) {
			$query .= " WHERE ({$where}) ";
		} elseif ($where && is_array($where)) {
			$wc = $this->parseWhereBindParams($where);
			$query .= " WHERE {$wc['clauses_and']}";
			$params = array_merge($params, $wc['params']);
		}

		if (isset($params['order_by']) && isset($params['order'])) {
			$query .= " ORDER BY {$this->quoteCol($params['order_by'])} {$params['order']} ";
		}

		if (isset($params['limit']) && isset($params['offset'])) {
			$query .= " LIMIT {$params['offset']}, {$params['limit']} ";
		}

		// unset all the shit that is not necessary for binding
		unset($params['cols'], $params['order_by'], $params['order'], $params['limit'], $params['offset']);

		$stmt = $this->pdo->prepare($query);
		$stmt->execute($params);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function findOne($table_name, $where = null, $cols = array()) {
		$rows = $this->find($table_name, $where, array('cols' => $cols, 'limit' => 1, 'offset' => 0));
		if ($rows) {
			return $rows[0];
		}
		return $rows;
	}

	/**
	 * Execute any query
	 *
	 * @param string $query Query string with optional placeholders
	 * @param array $params An array of parameters to bind to PDO statement
	 * @return array();
	 */
	public function execute($query, $params = array()) {
		$stmt = $this->pdo->prepare($query);
		$stmt->execute($params);
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * Count
	 *
	 * @param string $table_name
	 * @param array $where
	 * @return int
	 */
	public function count($table_name, $where = array()) {
		$query = "SELECT count(*) FROM {$table_name}";
		$params = array();
		if ($where && is_array($where)) {
			$wc = $this->parseWhereBindParams($where);
			$query .= " WHERE {$wc['clauses_and']}";
			$params = $wc['params'];
		}

		$stmt = $this->pdo->prepare($query);
		$stmt->execute($params);
		return $stmt->fetchColumn();
	}

	/**
	 * Insert Data into a MySQL Table
	 *
	 * @param string $table_name Table Name
	 * @param array $data An associative array of data with keys representing column names
	 * @param array $types A numerically indexed array representing the data-types of the value in the $data array.
	 * @throws Exception
	 * @return mix Returns an integer if last insert id was set else returns null
	 */
	public function insert($table_name, array $data, array $types = array()) {
		if (!$this->checkTypeCount($data, $types)) {
			throw new Exception("Array count for data and data-types do not match");
		}

		$keys = array_map(array($this, 'pkey'), array_keys($data));
		$cols = array_map(array($this, 'quoteCol'), array_keys($data));

		$query = $this->replace("INSERT INTO %{table_name} (%{cols}) VALUES (%{values})", array(
			'cols' => implode(', ', $cols),
			'values' => implode(', ', $keys),
			'table_name' => $table_name,
		));

		/* @var $stmt PDOStatement */
		$stmt = $this->pdo->prepare($query);
		$stmt = $this->bindValues($stmt, $data, array_values($types), false, true);
		$stmt->execute();
		return $this->lastInsertId();
	}

	public function update($table_name, array $data, array $where, array $data_types = array(), array $where_types = array()) {
		if (!$this->checkTypeCount($data, $data_types)) {
			throw new Exception("Array count for data and data-types do not match");
		}

		if (!$this->checkTypeCount($where, $where_types)) {
			throw new Exception("Array count for where clause and where clause data-types do not match");
		}

		$cols = array_map(array($this, 'quoteCol'), array_keys($data));
		$cols_where = array_map(array($this, 'quoteCol'), array_keys($where));
		$set_values = $where_values = array();

		foreach ($cols as $col) {
			$set_values[] = "$col = ?";
		}

		foreach ($cols_where as $col) {
			$where_values[] = "$col = ?";
		}

		$query = $this->replace("UPDATE %{table_name} SET %{set_values} WHERE (%{where_values})", array(
			'where_values' => implode(' AND ', $where_values),
			'set_values' => implode(', ', $set_values),
			'table_name' => $table_name,
		));

		/* @var $stmt PDOStatement */
		$stmt = $this->pdo->prepare($query);
		$stmt = $this->bindValues($stmt, $data, array_values($data_types), true, true);
		$stmt = $this->bindValues($stmt, $where, array_values($where_types));
		$stmt->execute();
		return $stmt->rowCount();
	}

	public function delete($table_name, array $data, array $types = array()) {
		$cols = array_map(array($this, 'pCol'), array_keys($data));
		$query = $this->replace("DELETE FROM %{table_name} WHERE (%{values})", array(
			'values' => implode(' AND ', $cols),
			'table_name' => $this->quoteCol($table_name),
		));

		/* @var $stmt PDOStatement */
		$stmt = $this->pdo->prepare($query);
		$stmt = $this->bindValues($stmt, $data, array_values($types), false, true);
		$stmt->execute();
		return $stmt->rowCount();
	}

	/**
	 * Used for INSERT, UPDATE and DELETE
	 *
	 * @param string $query
	 * @return int
	 */
	public function exec($query) {
		return $this->pdo->exec($query);
	}

	/**
	 * Used for SELECT
	 *
	 * @param string $query
	 * @return PDOStatement
	 */
	public function query($query, $fetch_style = PDO::FETCH_ASSOC) {
		return $this->pdo->query($query)->fetchAll($fetch_style);
	}

	/**
	 * @param string $query
	 *
	 * @return PDOStatement
	 */
	public function rquery($query) { //secured query with prepare and execute
		$args = func_get_args();
		array_shift($args); //first element is not an argument but the query itself, should removed

		$stmt = $this->pdo->prepare($query);
		$stmt->execute($args);
		return $stmt;
	}

	/**
	 * Quote a string for sql query
	 *
	 * @param string $string
	 * @return string
	 */
	public function quote($string) {
		return $this->pdo->quote($string);
	}

	/**
	 * @return PDO
	 */
	public function pdo() {
		return $this->pdo;
	}

	public function lastInsertId() {
		return $this->pdo->lastInsertId();
	}

	public function getSelectCols(array $cols) {
		$select = array();
		foreach ($cols as $key => $val) {
			if (is_numeric($key)) {
				$select[] = $this->parseColName($val);
			} else {
				$select[] = $this->parseColName($key) . ' AS ' . $this->parseColName($val);
			}
		}
		return implode(', ', $select);
	}

	/**
	 * Assert the existence of rows in a table
	 *
	 * @param string $table_name
	 * @param array|string $where If a string is given, it must be properly escaped
	 * @return boolean
	 */
	public function entryExists($table_name, $where) {
		return $this->count($table_name, $where) > 0;
	}

	private function parseColName($string) {
		if (strpos($string, '.') !== false) {
			$string = explode('.', $string, 2);
			$tableName = $this->quoteCol($string[0]);
			$fieldName = $this->quoteCol($string[1]);
			return $tableName . '.' . $fieldName;
		}
		return $this->quoteCol($string);
	}

	/**
	 * 
	 * @param PDOStatement $stmt
	 * @param array $data
	 * @param array $types
	 * @param bool $numeric
	 * #param bool $reset
	 * @return PDOStatement
	 */
	private function bindValues($stmt, $data, $types, $numeric = true, $reset = false) {
		static $i;
		if ($reset || $i === null) {
			$i = 0;
		}
		foreach ($data as $key => $value) {
			$type = $this->default_type;
			if (isset($types[$i]) && isset($this->types[$types[$i]])) {
				$type = $this->types[$types[$i]];
			}
			$stmt->bindValue(($numeric ? $i + 1 : $this->pkey($key)), $value, $type);
			$i++;
		}
		return $stmt;
	}

	private function checkTypeCount(array $data, array $types) {
		if (!$types) {
			return true;
		}
		return count($types) === count($data);
	}

	private function pkey($key) {
		return ':' . $key;
	}

	private function pCol($col) {
		$col = "`$col` = :$col";
		return $col;
	}

	private function quoteCol($col) {
		$col = trim($col);
		$col = "`$col`";
		return $col;
	}

	private function parseWhereBindParams(array $array) {
		$cols = array_keys($array);
		$values = array_values($array);

		$qcols = array_map(array($this, 'pCol'), $cols);
		$binds = array();

		foreach ($cols as $i => $col) {
			$binds[$this->pkey($col)] = $values[$i];
		}
		return array(
			'clauses' => $qcols,
			'clauses_and' => implode(' AND ', $qcols),
			'params' => $binds,
		);
	}

	private function replace($string, $params = array()) {
		foreach ($params as $key => $value) {
			$key = "%{" . $key . "}";
			$string = str_replace($key, $value, $string);
		}
		return $string;
	}

}
