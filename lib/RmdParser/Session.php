<?php

/**
 * Based on CodeIgniter's Session class
 */
class RmdParser_Session {

	protected $sess_encrypt_cookie = false;
	protected $sess_expiration = 7200;
	protected $sess_expire_on_close = false;
	protected $sess_match_ip = false;
	protected $sess_match_useragent = true;
	protected $sess_cookie_name = 'rmdparser_session';
	protected $cookie_prefix = '';
	protected $cookie_path = '/';
	protected $cookie_domain = '';
	protected $cookie_secure = false;
	protected $sess_time_to_update = 300;
	protected $encryption_key = 'zjDaJANLe0A8Bb1upC2LG2l8Onnwr5rz';
	protected $flashdata_key = 'rmdparser_flash';
	protected $time_reference = 'time';
	protected $gc_probability = 5;
	protected $userdata = array();
	protected $now;
	protected $encrypt;
	protected static $instance = null;

	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new RmdParser_Session((array) RmdParser_Config::get('session'));
		}
		return self::$instance;
	}

	/**
	 * Session Constructor
	 *
	 * The constructor runs the session routines automatically
	 * whenever the class is instantiated.
	 */
	public function __construct($params = array()) {
		log_message('debug', "Session Class Initialized");

		// Set all the session preferences, which can either be set
		// manually via the $params array above or via the config file
		foreach (array('sess_encrypt_cookie', 'sess_expiration', 'sess_expire_on_close', 'sess_match_ip', 'sess_match_useragent', 'sess_cookie_name', 'cookie_path', 'cookie_domain', 'cookie_secure', 'sess_time_to_update', 'time_reference', 'cookie_prefix', 'encryption_key') as $key) {
			if (isset($params[$key])) {
				$this->{$key} = $params[$key];
			}
		}

		if ($this->encryption_key == '') {
			show_error('In order to use the Session class you are required to set an encryption key in your config file.');
		}

		// Do we need encryption? If so, load the encryption class
		if ($this->sess_encrypt_cookie == true) {
			$this->encrypt = new RmdParser_Encrypt($this->encryption_key);
		}

		// Set the "now" time.  Can either be GMT or server time, based on the
		// config prefs.  We use this to set the "last activity" time
		$this->now = $this->_get_time();

		// Set the session length. If the session expiration is
		// set to zero we'll set the expiration two years from now.
		if ($this->sess_expiration == 0) {
			$this->sess_expiration = (60 * 60 * 24 * 365 * 2);
		}

		// Set the cookie name
		$this->sess_cookie_name = $this->cookie_prefix . $this->sess_cookie_name;

		// Run the Session routine. If a session doesn't exist we'll
		// create a new one.  If it does, we'll update it.
		if (!$this->sess_read()) {
			$this->sess_create();
		} else {
			$this->sess_update();
		}

		// Delete 'old' flashdata (from last request)
		$this->_flashdata_sweep();

		// Mark all new flashdata as old (data will be deleted before next request)
		$this->_flashdata_mark();

		// Delete expired sessions if necessary
		$this->_sess_gc();

		log_message('debug', "Session routines successfully run");
	}

	public function cookie($name, $default = false) {
		// @todo add ->security->xss_clean function from codeigniter
		$value = (isset($_COOKIE[$name])) ? $_COOKIE[$name] : $default;
		return $value;
	}

	public function ip_address() {
		return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
	}

	public function user_agent() {
		return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : false;
	}

	/**
	 * Fetch the current session data if it exists
	 *
	 * @access	public
	 * @return	bool
	 */
	public function sess_read() {
		// Fetch the cookie
		$session = $this->cookie($this->sess_cookie_name);

		if ($session === false) {
			log_message('debug', 'A session cookie was not found.');
			return false;
		}

		// HMAC authentication
		$len = strlen($session) - 40;

		if ($len <= 0) {
			log_message('error', 'Session: The session cookie was not signed.');
			return false;
		}

		// Check cookie authentication
		$hmac = substr($session, $len);
		$session = substr($session, 0, $len);

		// Time-attack-safe comparison
		$hmac_check = hash_hmac('sha1', $session, $this->encryption_key);
		$diff = 0;

		for ($i = 0; $i < 40; $i++) {
			$xor = ord($hmac[$i]) ^ ord($hmac_check[$i]);
			$diff |= $xor;
		}

		if ($diff !== 0) {
			log_message('error', 'Session: HMAC mismatch. The session cookie data did not match what was expected.');
			$this->sess_destroy();
			return false;
		}

		// Decrypt the cookie data
		if ($this->sess_encrypt_cookie == true) {
			$session = $this->encrypt->decode($session);
		}

		// Unserialize the session array
		$session = $this->_unserialize($session);

		// Is the session data we unserialized an array with the correct format?
		if (!is_array($session) OR ! isset($session['session_id']) OR ! isset($session['ip_address']) OR ! isset($session['user_agent']) OR ! isset($session['last_activity'])) {
			$this->sess_destroy();
			return false;
		}

		// Is the session current?
		if (($session['last_activity'] + $this->sess_expiration) < $this->now) {
			$this->sess_destroy();
			return false;
		}

		// Does the IP Match?
		if ($this->sess_match_ip == true AND $session['ip_address'] != $this->ip_address()) {
			$this->sess_destroy();
			return false;
		}

		// Does the User Agent Match?
		if ($this->sess_match_useragent == true AND trim($session['user_agent']) != trim(substr($this->user_agent(), 0, 120))) {
			$this->sess_destroy();
			return false;
		}

		// Session is valid!
		$this->userdata = $session;
		unset($session);

		return true;
	}

	/**
	 * Write the session data
	 *
	 * @access	public
	 * @return	void
	 */
	public function sess_write() {
		$this->_set_cookie();
	}

	/**
	 * Create a new session
	 *
	 * @access	public
	 * @return	void
	 */
	public function sess_create() {
		$sessid = '';
		while (strlen($sessid) < 32) {
			$sessid .= mt_rand(0, mt_getrandmax());
		}

		// To make the session ID even more secure we'll combine it with the user's IP
		$sessid .= $this->ip_address();

		$this->userdata = array(
			'session_id' => md5(uniqid($sessid, true)),
			'ip_address' => $this->ip_address(),
			'user_agent' => substr($this->user_agent(), 0, 120),
			'last_activity' => $this->now,
			'user_data' => ''
		);

		// Write the cookie
		$this->_set_cookie();
	}

	/**
	 * Update an existing session
	 *
	 * @access	public
	 * @return	void
	 */
	public function sess_update() {
		// We only update the session every five minutes by default
		if (($this->userdata['last_activity'] + $this->sess_time_to_update) >= $this->now) {
			return;
		}

		// Save the old session id so we know which record to
		// update in the database if we need it
		$old_sessid = $this->userdata['session_id'];
		$new_sessid = '';
		while (strlen($new_sessid) < 32) {
			$new_sessid .= mt_rand(0, mt_getrandmax());
		}

		// To make the session ID even more secure we'll combine it with the user's IP
		$new_sessid .= $this->ip_address();

		// Turn it into a hash
		$new_sessid = md5(uniqid($new_sessid, true));

		// Update the session data in the session data array
		$this->userdata['session_id'] = $new_sessid;
		$this->userdata['last_activity'] = $this->now;

		// Write the cookie
		$this->_set_cookie(NULL);
	}

	/**
	 * Destroy the current session
	 *
	 * @access	public
	 * @return	void
	 */
	public function sess_destroy() {
		// Kill the cookie
		setcookie($this->sess_cookie_name, addslashes(serialize(array())), ($this->now - 31500000), $this->cookie_path, $this->cookie_domain, 0);
		// Kill session data
		$this->userdata = array();
	}

	/**
	 * Fetch a specific item from the session array
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function userdata($item) {
		return (!isset($this->userdata[$item])) ? false : $this->userdata[$item];
	}

	/**
	 * Fetch all session data
	 *
	 * @access	public
	 * @return	array
	 */
	public function all_userdata() {
		return $this->userdata;
	}

	/**
	 * Add or change data in the "userdata" array
	 *
	 * @access	public
	 * @param	mixed
	 * @param	string
	 * @return	void
	 */
	public function set_userdata($newdata = array(), $newval = '') {
		if (is_string($newdata)) {
			$newdata = array($newdata => $newval);
		}

		if (count($newdata) > 0) {
			foreach ($newdata as $key => $val) {
				$this->userdata[$key] = $val;
			}
		}

		$this->sess_write();
	}

	/**
	 * Delete a session variable from the "userdata" array
	 *
	 * @access	array
	 * @return	void
	 */
	public function unset_userdata($newdata = array()) {
		if (is_string($newdata)) {
			$newdata = array($newdata => '');
		}

		if (count($newdata) > 0) {
			foreach ($newdata as $key => $val) {
				unset($this->userdata[$key]);
			}
		}

		$this->sess_write();
	}

	/**
	 * Add or change flashdata, only available
	 * until the next request
	 *
	 * @access	public
	 * @param	mixed
	 * @param	string
	 * @return	void
	 */
	public function set_flashdata($newdata = array(), $newval = '') {
		if (is_string($newdata)) {
			$newdata = array($newdata => $newval);
		}

		if (count($newdata) > 0) {
			foreach ($newdata as $key => $val) {
				$flashdata_key = $this->flashdata_key . ':new:' . $key;
				$this->set_userdata($flashdata_key, $val);
			}
		}
	}

	/**
	 * Keeps existing flashdata available to next request.
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	public function keep_flashdata($key) {
		// 'old' flashdata gets removed.  Here we mark all
		// flashdata as 'new' to preserve it from _flashdata_sweep()
		// Note the function will return false if the $key
		// provided cannot be found
		$old_flashdata_key = $this->flashdata_key . ':old:' . $key;
		$value = $this->userdata($old_flashdata_key);

		$new_flashdata_key = $this->flashdata_key . ':new:' . $key;
		$this->set_userdata($new_flashdata_key, $value);
	}

	/**
	 * Fetch a specific flashdata item from the session array
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function flashdata($key) {
		$flashdata_key = $this->flashdata_key . ':old:' . $key;
		return $this->userdata($flashdata_key);
	}

	/**
	 * Identifies flashdata as 'old' for removal
	 * when _flashdata_sweep() runs.
	 *
	 * @access	private
	 * @return	void
	 */
	private function _flashdata_mark() {
		$userdata = $this->all_userdata();
		foreach ($userdata as $name => $value) {
			$parts = explode(':new:', $name);
			if (is_array($parts) && count($parts) === 2) {
				$new_name = $this->flashdata_key . ':old:' . $parts[1];
				$this->set_userdata($new_name, $value);
				$this->unset_userdata($name);
			}
		}
	}

	/**
	 * Removes all flashdata marked as 'old'
	 *
	 * @access	private
	 * @return	void
	 */
	private function _flashdata_sweep() {
		$userdata = $this->all_userdata();
		foreach ($userdata as $key => $value) {
			if (strpos($key, ':old:')) {
				$this->unset_userdata($key);
			}
		}
	}

	/**
	 * Get the "now" time
	 *
	 * @access	private
	 * @return	string
	 */
	private function _get_time() {
		if (strtolower($this->time_reference) == 'gmt') {
			$now = time();
			$time = mktime(gmdate("H", $now), gmdate("i", $now), gmdate("s", $now), gmdate("m", $now), gmdate("d", $now), gmdate("Y", $now));
		} else {
			$time = time();
		}

		return $time;
	}

	/**
	 * Write the session cookie
	 *
	 * @access	public
	 * @return	void
	 */
	public function _set_cookie($cookie_data = NULL) {
		if (is_null($cookie_data)) {
			$cookie_data = $this->userdata;
		}

		// Serialize the userdata for the cookie
		$cookie_data = $this->_serialize($cookie_data);

		if ($this->sess_encrypt_cookie == true) {
			$cookie_data = $this->encrypt->encode($cookie_data);
		}

		$cookie_data .= hash_hmac('sha1', $cookie_data, $this->encryption_key);

		$expire = ($this->sess_expire_on_close === true) ? 0 : $this->sess_expiration + time();

		// Set the cookie
		setcookie($this->sess_cookie_name, $cookie_data, $expire, $this->cookie_path, $this->cookie_domain, $this->cookie_secure);
	}

	/**
	 * Serialize an array
	 *
	 * This function first converts any slashes found in the array to a temporary
	 * marker, so when it gets unserialized the slashes will be preserved
	 *
	 * @access	private
	 * @param	array
	 * @return	string
	 */
	private function _serialize($data) {
		if (is_array($data)) {
			foreach ($data as $key => $val) {
				if (is_string($val)) {
					$data[$key] = str_replace('\\', '{{slash}}', $val);
				}
			}
		} else {
			if (is_string($data)) {
				$data = str_replace('\\', '{{slash}}', $data);
			}
		}

		return serialize($data);
	}

	/**
	 * Unserialize
	 *
	 * This function unserializes a data string, then converts any
	 * temporary slash markers back to actual slashes
	 *
	 * @access	private
	 * @param	array
	 * @return	string
	 */
	private function _unserialize($data) {
		$data = @unserialize(strip_slashes($data));

		if (is_array($data)) {
			foreach ($data as $key => $val) {
				if (is_string($val)) {
					$data[$key] = str_replace('{{slash}}', '\\', $val);
				}
			}

			return $data;
		}

		return (is_string($data)) ? str_replace('{{slash}}', '\\', $data) : $data;
	}

	/**
	 * Garbage collection
	 *
	 * This deletes expired session rows from database
	 * if the probability percentage is met
	 *
	 * @access	public
	 * @return	void
	 */
	public function _sess_gc() {
		
	}

}
