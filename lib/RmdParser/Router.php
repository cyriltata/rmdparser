<?php

class RmdParser_Router {

	protected static $instance = null;
	protected $controllers;
	protected $subdirs = array();
	public $controller;
	public $action;
	public $parameters;
	public $current_url;
	public $query_string;
	public $controllerFile;
	public $controllerName;
	public $actionName;

	public function __construct() {
		$this->controllers = RMDP_APP_DIR . '/controllers';
	}

	/**
	 * @return RmdParser_Router
	 */
	public function parseRoute($uri = '') {
		/**
		 * Routes are assumed to be of the following form:
		 * /controller/action/param1/param2/param3/..
		 * where param1..paramN will be pased to the action of the specified controller
		 * If the controller or action cannot be gotten both will default to 'index'
		 */
		// First remove query strings if they are present
		$qs = explode('?', $uri);
		$curi = trim($qs[0], "/");
		$params = explode("/", $curi);

		// check if we are in a directory in the controllers folder and shift one step foward
		$controller = array_shift($params);
		while ($controller && is_dir($this->controllers . '/' . $controller)) {
			$this->controllers = $this->controllers . '/' . $controller;
			$this->subdirs[] = $controller;
			$controller = array_shift($params);
		}
		$action = array_shift($params);

		if (!$controller) {
			$controller = 'index';
		}

		if (!$action) {
			$action = 'index';
		}

		if (!self::isValidRname($controller) || !self::isValidRname($action)) {
			throw new RmdParser_Exception("Invalid controller/action name. Only alphanumeric characters allowed");
		}

		$controllerName = $this->getControllerName($controller);
		$controllerFile = $this->getControllerFile($controllerName);
		$actionName = $this->getActionName($action);

		if (!file_exists($controllerFile) && !$this->subdirs) {
			// add back action name as param
			array_unshift($params, $action);
			// asssume user was default to index controller and accessing an action
			$rp = $this->resetPath($controller);
			extract($rp);
			if (!file_exists($controllerFile)) {
				throw new RmdParser_Exception("Controller $controllerName does not exist. Expected to see controller in $controllerFile (NSD)");
			}
		} elseif (!file_exists($controllerFile) && $this->subdirs) {
			// add back action name as param
			array_unshift($params, $action);
			$rp = $this->resetPath($controller);
			extract($rp);
			if (!file_exists($controllerFile)) {
				throw new RmdParser_Exception("Controller $controllerName does not exist. Expected to see controller in $controllerFile (NSD)");
			}
		} elseif (!file_exists($controllerFile)) {
			throw new RmdParser_Exception("Controller $controllerName does not exist. Expected to see controller in $controllerFile");
		}

		if ($params === array('index')) {
			$params = array();
		}

		$this->action = $action;
		$this->controller = $controller;
		$this->parameters = $params;

		$this->query_string = $this->parseQueryString();
		$this->current_url = $this->parseCurrentUrl();

		$this->controllerName = $controllerName;
		$this->controllerFile = $controllerFile;

		$this->actionName = $actionName;
		return $this;
	}

	private function resetPath($controller) {
		$action = $controller;
		$actionName = $this->getActionName($action);

		$controller = 'index';
		$controllerName = $this->getControllerName($controller);
		$controllerFile = $this->getControllerFile($controllerName);
		return compact('action', 'actionName', 'controller', 'controllerName', 'controllerFile');
	}

	private function parseQueryString() {
		$string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : false;
		if (!$string) {
			return false;
		}
		$parts = explode('&', $string);
		array_shift($parts);
		return implode('&', $parts);
	}

	private function parseCurrentUrl() {
		$controller = $action = '';
		$segments = $this->subdirs;
		if ($this->controller != 'index') {
			$segments[] = $this->controller;
		}
		if ($this->action != 'index') {
			$segments[] = $this->action;
		}
		$segments = array_merge($segments, $this->parameters);
		$uri = implode("/", $segments);

		return site_url($uri, false, $this->query_string);
	}

	private function getControllerName($controller) {
		return ucwords($controller) . 'Controller';
	}

	private function getActionName($action) {
		$params = explode('-', $action);
		$name = strtolower(array_shift($params));
		foreach ($params as $n) {
			$name .= ucwords(strtolower($n));
		}
		return $name . 'Action';
	}

	private function getControllerFile($name) {
		return $this->controllers . '/' . $name . '.php';
	}

	public static function isValidRname($name) {
		return preg_match('/^[a-z0-9_-]+$/ui', $name);
	}

	/**
	 * @return RmdParser_Router
	 */
	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

}
