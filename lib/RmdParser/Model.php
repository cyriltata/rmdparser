<?php

class RmdParser_Model {

	/**
	 * @var RmdParser_Db
	 */
	protected $db;

	/**
	 * @var RmdParser_Session
	 */
	protected $session;
	protected $table_name = null;
	protected $id = null;

	protected function __construct() {
		$this->db = RmdParser::getDatabase();
		$this->session = RmdParser::getSession();
	}

	/**
	 * Sets the properties of an object and returns the object
	 *
	 * @param array $properties
	 * @return RmdParser_Model|Child
	 */
	public function setProperties(array $properties) {
		foreach ($properties as $property => $value) {
			if (property_exists($this, $property)) {
				$this->$property = $value;
			}
		}
		return $this;
	}

	/**
	 * Create a unique code for this model entity
	 *
	 * @return string
	 * @throws RmdParser_Exception
	 */
	public function createCode() {
		$code = generate_code();
		$exit_on = 10;
		$i = 0;
		while ($count = $this->db->count($this->table_name, array('code' => $code))) {
			$code = generate_code();
			$i++;
			if ($i >= $exit_on) {
				throw new RmdParser_Exception("Could not generate code for {$this->table_name} after {$exit_on} tries.");
			}
		}
		return $code;
	}

	public function exists($search) {
		return $this->db->count($this->table_name, $search) > 0;
	}

	public function create($data, $data_types = array()) {
		$this->id = $this->db->insert($this->table_name, $data, $data_types);
		if ($this->id) {
			$this->setProperties($data);
			return $this->id;
		}
		return null;
	}

	public function getPath($user = null) {
		if ($user === null) {
			$user = RmdParser_Model_User::getFromSession();
		}
		if (empty($this->filename) || !$user) {
			return null;
		}
		return $user->getDataDir() . '/' . $this->filename;
	}

	public function getLink($text = 'view/download', $class = "") {
		if (empty($this->filename)) {
			return null;
		}

		return sprintf('<a href="%s" target="_blank" class="%s">%s</a>', $this->getUrl(), $class, $text);
	}

	public function getUrl() {
		if (empty($this->filename)) {
			return null;
		}

		return site_url('file/' . $this->code, false);
	}

	public static function classMaps($className, array $data) {
		$map = array();
		foreach ($data as $i => $classProperties) {
			$map[$i] = self::classMap($className, $classProperties);
		}
		return $map;
	}

	public static function classMap($className, $classProperties) {
		if (class_exists($className, true)) {
			$class = new $className();
			$class->setProperties($classProperties);
			return $class;
		}
	}

	public static function getAllUploads($user = '', $offset = 0) {
		$db = RmdParser::getDatabase();

		$selects = array(
			'uploads.id' => 'upload_id',
			'uploads.code' => 'upload_code',
			'uploads.created' => 'upload_created',
			'uploads.filename' => 'upload_filename',
			'files.id' => 'file_id',
			'files.code' => 'file_code',
			'files.created' => 'file_created',
			'files.filename' => 'file_filename',
			'username',
			'original_filename',
			'type',
			'state'
		);

		$COLS = $db->getSelectCols($selects);
		$WHERE = '';
		$params = array();
		if ($user) {
			$params = array(':user_code' => $user);
			$WHERE = 'WHERE users.code = :user_code';
		}

		$query = "
            SELECT $COLS FROM uploads
            LEFT JOIN files ON uploads.id = files.upload_id 
			LEFT JOIN users ON uploads.user_id = users.id 
            $WHERE
            ORDER BY uploads.created DESC
        ";

		return $db->execute($query, $params);
	}

	public static function getUserDataDir($username) {
		$data_dir = RmdParser_Config::get('data_dir');
		if (!$data_dir) {
			throw new RmdParser_Exception("Configure data directory in config[data_dir]");
		}
		return $data_dir . '/' . $username;
	}

	protected function fetch($where, $cols = array()) {
		$data = $this->db->findOne($this->table_name, $where, $cols);
		if ($data) {
			$this->setProperties($data);
		}
		return $data;
	}

	protected function update($data, $data_types = array(), $where = array(), $where_types = array()) {
		if (!$where) {
			$where = array('id' => $this->id);
			$where_types = array('int');
		}
		return $this->db->update($this->table_name, $data, $where, $data_types, $where_types);
	}

	protected function delete($where = array(), $where_types = array()) {
		if (!$where) {
			$where = array('id' => $this->id);
			$where_types = array('int');
		}
		return $this->db->delete($this->table_name, $where, $where_types);
	}

}
