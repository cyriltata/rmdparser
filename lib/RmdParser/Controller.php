<?php

class RmdParser_Controller {

	/**
	 * @var RmdParser_Request
	 */
	protected $request;

	/**
	 * @var RmdParser_Db
	 */
	protected $db;

	/**
	 * @var RmdParser_Session
	 */
	protected $session;

	/**
	 * @var RmdParser_Router
	 */
	protected $router;
	protected $vars = array();
	protected $errors = array();
	protected $success = array();
	protected $rendered_vars = array();

	const FLASH_MSG_KEY = 'RmdParser_FM';

	public function __construct() {
		$this->request = RmdParser::getRequest();
		$this->db = RmdParser::getDatabase();
		$this->session = RmdParser::getSession();
		$this->router = RmdParser_Router::getInstance();

		$this->setVar('user', RmdParser_Model_User::getFromSession());
		$this->getFlashMessage();
	}

	public function indexAction() {
		throw new RmdParser_Exception("Must override indexAction method");
	}

	public function logoutAction() {
		RmdParser_Model_User::destroySession();
		redirect('login');
	}

	public function isAuthenticatedSession() {
		return RmdParser_Model_User::getFromSession();
	}

	protected function renderView($view, $vars = array()) {
		$this->setVar('errors', $this->getErrors());
		$this->setVar('success', $this->getSuccess());

		$this->rendered_vars = $vars = array_merge($this->rendered_vars, $this->vars, $vars);
		$view_file = RMDP_APP_DIR . '/views/' . $view . '.php';
		if (!file_exists($view_file)) {
			throw new RmdParser_Exception("Unable to find view $view. \n File Expected in $view_file");
		}
		extract($vars);
		include $view_file;
		return true;
	}

	protected function renderJSON($data = array('success' => false)) {
		header("Content-Type: application/json");
		echo json_encode($data);
		exit;
	}

	protected function setVar($key, $value) {
		if (!$key) {
			return;
		}
		$this->vars[$key] = $value;
	}

	protected function setVars(array $array) {
		foreach ($array as $key => $value) {
			$this->setVar($key, $value);
		}
	}

	protected function getVars($item = null) {
		if ($item && isset($this->vars[$item])) {
			return $this->vars[$item];
		} elseif ($item === null) {
			return $this->vars;
		}
	}

	protected function setError($value) {
		if (!$value) {
			return;
		}
		$this->errors[] = $value;
	}

	protected function setSuccess($value) {
		if (!$value) {
			return;
		}
		$this->success[] = $value;
	}

	protected function getErrors() {
		return $this->errors;
	}

	protected function getSuccess() {
		return $this->success;
	}

	protected function setMessage($message, $type = 'success') {
		if ($type === 'success') {
			return $this->setSuccess($message);
		}
		$this->setError($message);
	}

	protected function setFlashMessage($message, $type = 'success') {
		$data = array('t' => $type, 'm' => $message);
		$this->session->set_userdata(self::FLASH_MSG_KEY, json_encode($data));
	}

	protected function getFlashMessage() {
		$message = json_decode($this->session->userdata(self::FLASH_MSG_KEY));
		$this->session->unset_userdata(self::FLASH_MSG_KEY);
		if (!empty($message->t) && !empty($message->m)) {
			$this->setMessage($message->m, $message->t);
		}
	}

	protected function downloadFile($file, $type) {
		$filename = basename($file);
		$filesize = filesize($file);
		header('Content-Description: File Transfer');
		header('Content-Type: ' . $type);
		header('Content-Disposition: attachment; filename = "' . $filename . '"');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		if ($filesize) {
			header('Content-Length: ' . $filesize);
		}
		readfile($file);
		exit(0);
	}

	/**
	 * @return RmdParser_Model_User
	 */
	protected function getUser() {
		return $this->getVars('user');
	}

}
