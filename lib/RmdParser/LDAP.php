<?php

class RmdParser_LDAP {

	protected static $instances = array();
	protected $connection;
	protected $entries;
	protected $config;

	/**
	 * @param string $instance
	 * @return RmdParser_LDAP
	 */
	public static function getInstance($instance = 'ldap') {
		if (!isset(self::$instances[$instance])) {
			$config = RmdParser_Config::get($instance);
			self::$instances[$instance] = new self($config);
		}
		return self::$instances[$instance];
	}

	protected function __construct(array $config) {
		$hostname = isset($config['protocol']) ? "{$config['protocol']}://{$config['host']}" : $config['host'];

		if (!empty($config['port'])) {
			$this->connection = ldap_connect($hostname, (int) $config['port']);
		} else {
			$this->connection = ldap_connect($hostname);
		}

		if (!$this->connection) {
			throw new RmdParser_Exception(ldap_error($this->connection), ldap_errno($this->connection));
		}

		$this->set_option(LDAP_OPT_PROTOCOL_VERSION, 3);
		$this->set_option(LDAP_OPT_REFERRALS, 0);

		if (!empty($config['user']) && !empty($config['password'])) {
			$this->bind($config['user'], $config['password']);
		}
		$this->config = $config;
	}

	/**
	 * Set the value of the given option.
	 *
	 * @link http://php.net/manual/en/function.ldap-set-option.php
	 * @param int $option
	 * @param mixed $value The new value for the specified <i>option</i>.
	 * @throws RmdParser_Exception
	 */
	public function set_option($option, $value) {
		if (!ldap_set_option($this->connection, $option, $value)) {
			throw new RmdParser_Exception(ldap_error($this->connection), ldap_errno($this->connection));
		}
	}

	/**
	 * Bind to LDAP directory
	 *
	 * @link http://php.net/manual/en/function.ldap-bind.php
	 * @param string $bind_rdn [optional]
	 * @param string $bind_password [optional]
	 * @throws RmdParser_Exception
	 */
	public function bind($bind_rdn = null, $bind_password = null) {
		if (!ldap_bind($this->connection, $bind_rdn, $bind_password)) {
			throw $this->wrapError(__FUNCTION__, $bind_rdn);
		}
	}

	/**
	 * Search LDAP tree
	 *
	 * @link http://php.net/manual/en/function.ldap-search.php
	 * @param string $base_dn The base DN for the directory.
	 * @param string $filter
	 * @param array $attributes [optional]
	 * @param int $attrsonly [optional]
	 * @param int $sizelimit [optional]
	 * @param int $timelimit [optional]
	 * @param int $deref [optional]
	 * @return db_pool_ldap_result
	 * @throws RmdParser_Exception
	 */
	public function search($base_dn, $filter, array $attributes = array(), $attrsonly = null, $sizelimit = null, $timelimit = null, $deref = null) {
		$this->entries = null;
		$ldapsearch = ldap_search($this->connection, $base_dn, $filter, $attributes, $attrsonly, $sizelimit, $timelimit, $deref);
		if (!$ldapsearch) {
			throw new RmdParser_Exception(ldap_error($this->connection), ldap_errno($this->connection));
		}

		$this->entries = ldap_get_entries($this->connection, $ldapsearch);
		if (!$this->entries) {
			throw new RmdParser_Exception(ldap_error($this->connection), ldap_errno($this->connection));
		}
		return $this->entries;
	}

	public function getEntries() {
		return $this->entries;
	}

	public function count() {
		return isset($this->entries['count']) ? $this->entries['count'] : 0;
	}

	public function setConfigs(array $config) {
		$this->config = $config;
	}

	public function getConfigs() {
		return $this->config;
	}

	public function getConfig($key) {
		return (isset($this->config[$key])) ? $this->config[$key] : null;
	}

	public function __destruct() {
		if ($this->connection) {
			ldap_close($this->connection);
		}
	}

	/**
	 * Helper method to format errors to Exception
	 *
	 * @param string $call
	 * @return RmdParser_Exception
	 */
	private function wrapError($call) {
		$args = func_get_args();
		$error = "$call: " . ldap_error($this->connection);
		if (func_num_args() > 1) {
			array_shift($args);
			$error .= ": " . join(', ', $args);
		}
		return new RmdParser_Exception($error, ldap_errno($this->connection));
	}

}
