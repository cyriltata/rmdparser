<?php

class RmdParser_Model_User extends RmdParser_Model {

	public $id;
	public $code;
	public $username;
	public $first_name;
	public $last_name;
	public $email;
	public $created;
	public $last_login;
	public $is_student = 0;
	public $is_staff = 0;
	public $is_admin = 0;
	protected $table_name = 'users';
	public static $instances = array();

	const SESSION_KEY = 'RmdParser_USER';

	/**
	 * @var RmdParser_LDAP
	 */
	protected $ldap;

	/**
	 * 
	 * @param string $instance
	 * @param array $auth_data [optional]
	 * @return RmdParser_Model_User
	 */
	public static function getInstance($instance, $auth_data = array()) {
		if (!isset(self::$instances[$instance])) {
			self::$instances[$instance] = new self($auth_data);
		}
		return self::$instances[$instance];
	}

	/**
	 * 
	 * @param int $id
	 * @return RmdParser_Model_User
	 */
	public static function getById($id) {
		$dbh = RmdParser::getDatabase();
		$user = $dbh->findOne('users', array('id' => (int) $id));
		if ($user) {
			return self::getFromData($user);
		}
	}

	private static function setInstance($instance, RmdParser_Model_User $user) {
		self::$instances[$instance] = $user;
	}

	/**
	 * @return mixed Returns a RmdParser_Model_User is user was found in session or FALSE otherwise
	 */
	public static function getFromSession() {
		$code = RmdParser::getSession()->userdata('logged');
		$username = RmdParser::getSession()->userdata('user');
		if (!$code || !$username) {
			return false;
		}

		// check if user is saved instances
		$instance = self::getInstance($username);
		if ($instance->code) {
			return $instance;
		}

		$data = $instance->fetch(array('code' => $code));
		if (!$data) {
			log_message('debug', 'User found in session but not in db (strange)');
			return false;
		}

		return self::getFromData($data);
	}

	public static function destroySession() {
		$sess_data = array(
			'logged' => '',
			'user' => '',
		);
		RmdParser::getSession()->set_userdata($sess_data);
		RmdParser::getSession()->sess_destroy();
	}

	protected static function getFromData($data) {
		$user = new self();
		$user->setProperties((array) $data);
		self::setInstance($user->username, $user);
		return self::getInstance($user->username);
	}

	protected function __construct($auth = array()) {
		parent::__construct();
		if ($auth) {
			$this->authenticateLDAP($auth);
		}
	}

	public function authenticateLDAP(array $auth) {
		if (!$auth['username'] || !$auth['password']) {
			throw new RmdParser_Exception("Please provide a username and password");
		}

		$username = $auth['username'];
		try {
			$this->ldap = RmdParser_LDAP::getInstance();
			$ldap_query = array(
				"(objectClass=person)",
				"(uid=$username)",
			);
			$filter = "(&" . implode('', $ldap_query) . ")";
			$this->ldap->search($this->ldap->getConfig('base_dn'), $filter);

			if ($this->ldap->count()) {
				$entries = $this->ldap->getEntries();
				$auth['username'] = $entries[0]['dn'];
				$this->ldap->bind($auth['username'], $auth['password']);
				$this->parseLDAPInfo($entries[0]);
			} else {
				throw new RmdParser_Exception("No user found with username '{$username}'");
			}
		} catch (Exception $ex) {
			// you might be catching things from LDAP class
			throw new RmdParser_Exception("No user found with username '{$username}'");
		}

		return $this->createSession();
	}

	public function toArray() {
		return array(
			'username' => $this->username,
			'first_name' => $this->first_name,
			'last_name' => $this->last_name,
			'email' => $this->email,
			'is_student' => $this->is_student,
			'is_staff' => $this->is_staff,
		);
	}

	public function getNames() {
		return $this->last_name . ' ' . $this->first_name;
	}

	public function getFiles($offset = 0) {
		$selects = array(
			'uploads.id' => 'upload_id',
			'uploads.code' => 'upload_code',
			'uploads.created' => 'upload_created',
			'uploads.filename' => 'upload_filename',
			'files.id' => 'file_id',
			'files.code' => 'file_code',
			'files.created' => 'file_created',
			'files.filename' => 'file_filename',
			'original_filename',
			'type',
			'state'
		);

		$select_fields = $this->db->getSelectCols($selects);
		$query = "
            SELECT {$select_fields} FROM uploads
            LEFT JOIN files ON uploads.id = files.upload_id 
            WHERE uploads.user_id = :user_id
            ORDER BY uploads.created DESC
        ";

		return $this->db->execute($query, array(':user_id' => $this->id));
	}

	private function parseLDAPInfo($info) {
		//var_dump($info); die();
		$user = array();
		$user['username'] = $info['uid'][0];
		$user['email'] = $info['cn'][0] . $this->ldap->getConfig('mail_suffix');
		$user['last_name'] = $info['sn'][0];
		$user['first_name'] = $info['givenname'][0];
		$this->setProperties($user);
	}

	/**
	 * Save user data in session
	 *
	 * @param array $user
	 */
	private function createSession() {
		// If user does not exist in db, create one
		if (!$this->exists(array('username' => $this->username))) {
			$data = array(
				'code' => $this->createCode(),
				'username' => $this->username,
				'first_name' => $this->first_name,
				'last_name' => $this->last_name,
				'email' => $this->email,
				'created' => time(),
				'last_login' => time(),
				'is_student' => $this->is_student,
				'is_staff' => $this->is_staff,
			);
			$data_types = array('str', 'str', 'str', 'str', 'str', 'int', 'int', 'int', 'int');
			$id = $this->create($data, $data_types);
			if ($id) {
				$data['id'] = $id;
			}
		} else {
			$data = $this->fetch(array('username' => $this->username));
			$this->setProperties($data);
		}

		$data['last_login'] = time();
		$this->update(array('last_login' => $data['last_login']), array('int'));
		$this->setProperties($data);
		$sess_data = array(
			'logged' => $this->code,
			'user' => $this->username,
		);

		$this->session->set_userdata($sess_data);
		$this->createDataDir();
		return $this;
	}

	private function createDataDir() {
		$data_dir = RmdParser_Config::get('data_dir');
		if (!$data_dir) {
			throw new RmdParser_Exception("Configure data directory in config[data_dir]");
		}
		$user_dir = $data_dir . '/' . $this->username;
		if (!is_dir($user_dir) && !mkdir($user_dir, 0775, true)) {
			throw new RmdParser_Exception("Unable to create data directory for user {$this->username}");
		}
		return $user_dir;
	}

	public function getDataDir() {
		return RmdParser_Model::getUserDataDir($this->username);
	}

	public function canModify(RmdParser_Model $file) {
		if ($this->is_admin) {
			return true;
		}

		return $this->id == $file->user_id;
	}

}
