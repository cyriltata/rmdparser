<?php

class RmdParser_Model_Partnershipstudy extends RmdParser_Model {

	const EMAIL_TEMPLATE_BODY_FILE = RMDP_DIR . '/logs/partnerstudy-email-body.txt';
	const EMAIL_TEMPLATE_SUBJECT_FILE = RMDP_DIR . '/logs/partnerstudy-email-subject.txt';

	public function __construct() {
		parent::__construct();
	}

	public function saveParticipantsData($data) {
		$noRows = count($data);
		$start = 0;
		$limit = 10;

		$row_value = '(' . implode(',', array_fill(0, 7, '?')) . ')';
		$row_values = array_fill(0, $limit, $row_value);

		$sql_tpl = '
			INSERT INTO partnerstudy_participants (id, init_date, first_name, last_name, email, study, gender_greet)
			VALUES %s
			ON DUPLICATE KEY UPDATE 
			init_date = VALUES(init_date), first_name = VALUES(first_name), last_name = VALUES(last_name), email = VALUES(email), study = VALUES(study), gender_greet = VALUES(gender_greet)
		';
		$sql = sprintf($sql_tpl, implode(',', $row_values));

		$pdo = $this->db->pdo();
		try {
			$pdo->beginTransaction();
			$stmt = $pdo->prepare($sql);
			while ($start < $noRows) {
				$slice = array_slice($data, $start, $limit);
				if (count($slice) < $limit) {
					// refresh SQL and PDO statement to support a smaller number of values
					$row_values = array_fill(0, count($slice), $row_value);
					$sql = sprintf($sql_tpl, implode(',', $row_values));
					$stmt->closeCursor();
					$stmt = $pdo->prepare($sql);
				}

				$values = $this->insertRowValues($slice);
				$stmt->execute($values);
				$start += $limit;
			}
			$pdo->commit();
			return true;
		} catch (Exception $e) {
			$pdo->rollBack();
			log_exception($e);
			return false;
		}
	}

	protected function insertRowValues($rows) {
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($rows));
		return iterator_to_array($it, false);
	}

	/**
	 * Get all participants statement
	 *
	 * @return PDOStatement
	 */
	public function getParticipants() {
		$stmt = $this->db->pdo()->prepare('SELECT id, init_date, first_name, last_name, email, invitation_sent, study, gender_greet FROM partnerstudy_participants');
		$stmt->execute();
		return $stmt;
	}

	public function sendInvitation($participantId) {
		$email = $this->db->findOne('partnerstudy_participants', array('id' => $participantId));
		if (!$email) {
			return false;
		}
		$email['study_label'] = partnershipstudy_get_study_label($email['study']);

		$account = (array) RmdParser_Config::get('partnership_study_email');
		$mail = new PHPMailer();
		$mail->SetLanguage("de", "/");

		$mail->isSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPKeepAlive = true;
		$mail->Mailer = "smtp";
		$mail->Host = $account['host'];
		$mail->Port = $account['port'];
		if ($account['tls']) {
			$mail->SMTPSecure = 'tls';
		} else {
			$mail->SMTPSecure = 'ssl';
		}
		$mail->Username = $account['username'];
		$mail->Password = $account['password'];

		$mail->setFrom($account['from'], $account['from_name']);
		$mail->AddReplyTo($account['from'], $account['from_name']);
		$mail->CharSet = "utf-8";
		$mail->WordWrap = 65;
		$mail->AllowEmpty = true;
		$mail->isHTML(true);

		if (is_array(RmdParser_Config::get('partnership_study_email.smtp_options'))) {
			$mail->SMTPOptions = array_merge($mail->SMTPOptions, RmdParser_Config::get('partnership_study_email.smtp_options'));
		}

		$emailContent = @file_get_contents(RmdParser_Model_Partnershipstudy::EMAIL_TEMPLATE_BODY_FILE);
		$emailSubject = @file_get_contents(RmdParser_Model_Partnershipstudy::EMAIL_TEMPLATE_SUBJECT_FILE);
		$emails = explode(';', $email['email']);
		$email['recipient'] = end($emails);
		$email['subject'] = trim($emailSubject);
		$email['message'] = partnershipstudy_email_template($emailContent, $email);
		
		$mail->Subject = $email['subject'];
		$mail->msgHTML($email['message']);
		$mail->addAddress($email['recipient']);

		return $mail->send();
	}

}
