<?php

class RmdParser_Model_Upload extends RmdParser_Model {

	public $id;
	public $code;
	public $user_id;
	public $filename;
	public $created;
	public $original_filename;
	protected $table_name = 'uploads';
	protected $parse_report = null;
	protected $parse_success_data = array();
	protected $parse_error_data = array();

	public function __construct($code = '') {
		parent::__construct();
		if ($code) {
			$this->fetch(array('code' => $code));
		}
	}

	public function generateNewFilename($filename) {
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		return md5($filename . microtime(true) . uniqid('rmdparser')) . '.' . $ext;
	}

	public function parse($force = false) {
		$this->parse_reset();

		$user = RmdParser_Model_User::getById($this->user_id);
		if (!$user) {
			throw new Exception("User associated to file could not be determined");
		}

		$file = $user->getDataDir() . '/' . $this->filename;
		if (!is_file($file)) {
			throw new RmdParser_Exception("Unable to get find Rmd file");
		}

		if ($this->user_id != $user->id) {
			throw new Exception("You do not have permission to parse this file");
		}

		/** @var $files RmdParser_Model_File[] */
		if ($force === true) {
			$files = $this->getFiles();
		} else {
			$files = $this->getUncompletedFiles();
		}

		if (!$files && !$this->hasFiles()) {
			// Create to be parsed files for this upload
			$files = array();
			$supported = RmdParser_Model_File::$TYPES_SUPPORTED;
			foreach ($supported as $type) {
				$files[] = $this->createFile($type, $user);
			}
		}

		if (!$files) {
			$this->parse_error_data[] = sprintf("No waiting files were found for %s", $this->original_filename);
			return false;
		}

		foreach ($files as $file) {
			$parsed = $this->callOpenCPU($file, $user);
			if ($parsed === true) {
				$this->parse_success_data[] = sprintf("%s was converted to %s [%s]", $this->original_filename, strtoupper($file->type), $file->getLink());
			} else {
				$this->parse_error_data[] = sprintf("%s failed to convert to %s [error: %s]", $this->original_filename, strtoupper($file->type), $this->parse_report);
			}
		}
		return empty($this->parse_error_data);
	}

	public function getParseReport() {
		return $this->parse_report;
	}

	public function getParseSuccesses() {
		return $this->parse_success_data;
	}

	public function getParseErrors() {
		return $this->parse_error_data;
	}

	/**
	 * @return boolean
	 */
	public function hasFiles() {
		return $this->db->count('files', array('upload_id' => $this->id)) > 0;
	}

	/**
	 * @return boolean
	 */
	public function hasCompletedFiles() {
		return $this->db->count('files', array('upload_id' => $this->id, 'state' => RmdParser_Model_File::STATE_COMPLETED)) > 0;
	}

	/**
	 * Get files associated to this upload guy
	 * @param string $state [optional]
	 *
	 * @return RmdParser_Model_File[]
	 */
	public function getFiles($state = null, $where = null) {
		if ($where === null) {
			$where = array('upload_id' => $this->id);
			if ($state) {
				$where['state'] = $state;
			}
		}
		$files = RmdParser_Model_File::findBy($where);
		if (!$files) {
			return array();
		}
		return RmdParser_Model::classMaps('RmdParser_Model_File', $files);
	}

	public function getUncompletedFiles() {
		$where = sprintf("upload_id = %d AND state IN ('%s', '%s')", $this->id, RmdParser_Model_File::STATE_FAILED, RmdParser_Model_File::STATE_NEW);
		return $this->getFiles(null, $where);
	}

	private function parse_reset() {
		$this->parse_error_data = array();
		$this->parse_success_data = array();
		$this->parse_report = null;
	}

	private function createFile($type, $user) {
		$filename = pathinfo($this->filename, PATHINFO_FILENAME) . '.' . $type;
		$filedata = array(
			'code' => $this->createCode(),
			'upload_id' => $this->id,
			'user_id' => $user->id,
			'filename' => $filename,
			'type' => $type,
			'created' => time(),
			'state' => RmdParser_Model_File::STATE_NEW,
		);

		$file = new RmdParser_Model_File();
		$file->create($filedata);
		return $file;
	}

	/**
	 * Call render markdown method of open cpu for the specied file format
	 *
	 * @param RmdParser_Model_File $file
	 * @param RmdParser_Model_User $user
	 * @return boolean TRUE if file was coneverted FALSE otherwise
	 */
	private function callOpenCPU(RmdParser_Model_File $file, RmdParser_Model_User $user) {
		$this->parse_report = null;

		$openCPU = new RmdParser_OpenCPU(RmdParser_Config::get('opencpu_instance'));
		$files = $openCPU->rmarkdownRender($this->getPath(), $file->rmarkdownOutput());

		if (!$files) {
			$this->parse_report = $openCPU->getLastMessage();
			$file->update(array('state' => RmdParser_Model_File::STATE_FAILED));
			return false;
		}

		if (!isset($files[$file->type])) {
			$this->parse_report = "File type not found in opencpu response";
			$file->update(array('state' => RmdParser_Model_File::STATE_FAILED));
			return false;
		}

		// try to download file to local system
		try {
			$parsedFile = $files[$file->type];
			$destination = $user->getDataDir() . '/' . basename($parsedFile);
			RmdParser_CURL::DownloadUrl($parsedFile, $destination);
			$file->update(array('state' => RmdParser_Model_File::STATE_COMPLETED));
			return true;
		} catch (Exception $e) {
			$file->update(array('state' => RmdParser_Model_File::STATE_FAILED));
			$this->parse_report = $e->getMessage();
			return false;
		}
	}

}
