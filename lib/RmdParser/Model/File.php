<?php

class RmdParser_Model_File extends RmdParser_Model {

	public $id;
	public $code;
	public $user_id;
	public $upload_id;
	public $filename;
	public $type;
	public $state;
	public $created;
	protected $table_name = 'files';

	const STATE_COMPLETED = 'completed';
	const STATE_FAILED = 'failed';
	const STATE_NEW = 'new';
	const TYPE_HTML = 'html';
	const TYPE_PDF = 'pdf';
	const TYPE_RMD = 'rmd';
	const TYPE_TXT = 'txt';

	public static $TYPES_SUPPORTED = array(self::TYPE_HTML, self::TYPE_PDF);

	public function __construct($code = '') {
		parent::__construct();
		if ($code) {
			$this->fetch(array('code' => $code));
		}
	}

	public static function findBy($where, $params = array()) {
		return RmdParser::getDatabase()->find('files', $where, $params);
	}

	public function rmarkdownOutput() {
		return strtolower($this->type . '_document');
	}

	/*
	 * return the mime type of a file given filename
	 * @param string $filename
	 * @return mixed
	 */

	public static function getMime($filename) {
		$const = defined('FILEINFO_MIME_TYPE') ? FILEINFO_MIME_TYPE : FILEINFO_MIME;
		$mime = explode(';', self::getFileInfo($filename, $const));
		if (!$mime) {
			return false;
		}

		$mime_type = $mime[0];
		return $mime_type;
	}

	public static function getFileInfo($filename, $constant) {
		$finfo = finfo_open($constant);
		$info = finfo_file($finfo, $filename);
		finfo_close($finfo);

		return $info;
	}

}
