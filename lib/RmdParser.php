<?php

/**
 * Module to route and run a request
 * 
 */
class RmdParser {

	public static function run() {
		try {
			$request = self::getRequest();
			// Find which parameter has the root
			$params = array_keys($request->getParams());
			$route = array_shift($params);
			// Router

			$router = RmdParser_Router::getInstance()->parseRoute($route);

			require_once $router->controllerFile;
			$controllerClass = new $router->controllerName;

			if (!method_exists($controllerClass, $router->actionName)) {
				throw new RmdParser_Exception("Action {$router->actionName} does not exist in controller {$router->controllerName}");
			}

			//If we arrive here then all went well so call action in controller
			$caller = call_user_func_array(array($controllerClass, $router->actionName), $router->parameters);

			if ($caller === false) {
				throw new RmdParser_Exception("Parsing '$route' failed");
			}
		} catch (RmdParser_Exception $e) {
			log_exception($e);
			show_error($e->getMessage());
		} catch (Exception $e) {
			log_exception($e);
			show_error($e->getMessage());
		}
	}

	/**
	 * 
	 * @staticvar type $request
	 * @return RmdParser_Request
	 */
	public static function getRequest() {
		static $request = null;
		if ($request === null) {
			$request = new RmdParser_Request($_REQUEST);
		}
		return $request;
	}

	/**
	 * @return RmdParser_Db
	 */
	public static function getDatabase() {
		return RmdParser_Db::getInstance();
	}

	/**
	 * @return RmdParser_Session
	 */
	public static function getSession() {
		return RmdParser_Session::getInstance();
	}

	public function isCli() {
		if (php_sapi_name() == 'cli' && empty($_SERVER['REMOTE_ADDR'])) {
			return true;
		} else {
			return false;
		}
	}

}
