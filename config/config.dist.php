<?php
/** Config Parameters */

// Base URL to application's index file (WITHOUT PROTOCOL)
$config['base_url'] = 'rmdparser.org';
// used protocol
$config['protocol'] = 'http://';

// index file. If you use .htaccess to redirect requests then set this to an empty string
$config['index_file'] = '';//'index.php';

// Which environment is application currently running on One of 'prod', 'dev', 'test'
$config['environment'] = 'dev';

// Instances of open cpu to use in order to parse Rmd files
$config['opencpu_instance'] = 'https://opencpu.org';

// OpenLDAP server
$config['ldap'] = array(
	'protocol' => 'ldap',
	'host' => 'ldap.gwdg.de',
	'port' => '389',
	'base_dn' => 'OU=base,DC=ldap,DC=org',
	// user and password are necessary if ldap server does not allow anonymous searches
	'user' => 'cn=admin,ou=base,dc=ldap,dc=org',
	'password' => 'password',
);
// Databse connection settings
$config['database'] = array(
    'datasource' => 'Database/Mysql',
	'persistent' => false,
	'host' => 'localhost',
    'port' => '',
	'username' => 'root',
	'password' => '',
	'dbname' => 'rmdparser',
	'prefix' => '',
	'encoding' => 'utf8',
	'unix_socket' => '',
);

// Default time zone to use
$config['timezone'] = 'Europe/Berlin';

// Session settings
// This application creates and manages it's own session Data. i.e PHP default session is not used.
// To overwrite default settings uncomment and set value
$config['session'] = array(
//    'sess_encrypt_cookie' => false,
//    'sess_expiration' => '',
//    'sess_expire_on_close' => '',
//    'sess_match_ip' => '',
//    'sess_match_useragent' => '',
//    'sess_cookie_name' => '',
//    'cookie_path' => '',
//    'cookie_domain' => '',
//    'cookie_secure' => '',
//    'sess_time_to_update' => '',
//    'time_reference' => '',
//    'cookie_prefix' => '',
//    'encryption_key' => 'zjDaJANLe0A8Bb1upC2LG2l8Onnwr5rz',
);

// maximum file size upload (in (Example: 10M, 1024K)
$config['max_file_upload_size'] = '10M';

$config['error_log_file'] = RMDP_DIR . '/logs/error.log';

// base dir in which all data will be gotten
$config['data_dir'] = RMDP_DIR . '/data';

// Email account to send out invites for partnership study
// email SMTP configuration
$config['partnership_study_email'] = array(
	'subject' => 'Partner Study',
	'host' => 'smtp.gmail.com',
	'port' => 587,
	'tls' => true,
	'from' => 'example@gmail.com',
	'from_name' => 'Partnership Study',
	'username' => 'example@gmail.com',
	'password' => 'mypassword',
	'loop_rest_interval' => 10,
	'smtp_options' => array(
		'ssl' => array(
			'verify_peer' => false,
			'verify_peer_name' => false,
			'allow_self_signed' => true
		)
	),
	'log_file' => RMDP_DIR . '/logs/partnership_study.log',
);

$config['partnership_study_labels'] = array(
	'study_name' => 'Study Label',
);


